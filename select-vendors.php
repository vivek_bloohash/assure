<?php
session_start();
if (!isset($_SESSION['logged_in'])) {
    header("Location:index.php");
}

if (!isset($_POST['enqueiry_id'])) {
    header("Location:display_enquiries.php");
} else {
    $enqueiry_id = $_POST['enqueiry_id'];
}
date_default_timezone_set('Asia/Calcutta');
include 'api/web/dbconfig.php';
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Select Vendors</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/xtremeadmin/" />
    <!-- Custom CSS -->
    <link href="dist/css/style.min.css" rel="stylesheet">
    <link href="sweetalert/sweetalert.css" rel="stylesheet">
    <link href="assets/libs/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/libs/select2/dist/css/select2.min.css">
    <style>
    .select2-selection__choice {
        background-color: #326bfc !important;
    }

    #chk_list {
        height: 400px;
        overflow: hidden;
        overflow-y: scroll;
    }
    div#memListTable_paginate {
    margin-bottom: 30px;
}
a.paginate_button {
    padding: 4px 10px;
    background-color: #dddddd;
    margin-right: 4px !important;
    display: inline-block;
    cursor: pointer;
}
    </style>
    <!-- <link href="sweetalert/sweetalert.css" rel="stylesheet"> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include "header.php"; ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php include "sidebar.php"; ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Import Vendors</h4>

                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item"><a href="#">View Enquiry</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Select Vendors</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-7 align-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <div class="m-r-10">

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body align-center">
                                <!-- Add New Product -->
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="card card-body">
                                        <h4 class="card-title">Selected Vendors</h4>
                                        <?php
                                            $qry = "SELECT vendors FROM `customer_enquiries` where ce_id = '$enqueiry_id'";
                                            // echo $qry;
                                            $query = $connection->query($qry);
                                            if($query->num_rows > 0){
                                                while($row = $query->fetch_assoc()){
                                                $vendors = $row['vendors'];
                                                }
                                            }
                                            $final = str_replace(':', ' or id = ', $vendors);
                                            if($final != ''){
                                                $qry = "SELECT * FROM vendors where id = $final"; 
                                            ?>
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-bordered"
                                                        cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Name</th>
                                                                <th>Location</th>
                                                                <th>Product</th>
                                                                <th>Person</th>
                                                                <th>Email</th>
                                                                <th>Mobile1</th>
                                                                <th>Mobile2</th>
                                                                <th>Mobile3</th>
                                                                <th>Mobile4</th>
                                                                <th>Whatsapp</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        $query = $connection->query($qry);
                                                            if($query->num_rows > 0){
                                                                while($row = $query->fetch_assoc()){
                                                                    echo "<tr>";
                                                                    echo "<td>" . $row['name'] . "</td>";
                                                                    echo "<td>" . $row['location'] . "</td>";
                                                                    echo "<td>" . $row['product'] . "</td>";
                                                                    echo "<td>" . $row['person'] . "</td>";
                                                                    echo "<td>" . $row['email'] . "</td>";
                                                                    echo "<td>" . $row['mobile1'] . "</td>";
                                                                    echo "<td>" . $row['mobile2'] . "</td>";
                                                                    echo "<td>" . $row['mobile3'] . "</td>";
                                                                    echo "<td>" . $row['mobile4'] . "</td>";
                                                                    echo "<td>" . $row['whatsapp'] . "</td>";

                                                                    echo "</tr>";
                                                                }
                                                            }
                                                        ?>
                                                                
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <?php
                                            }
                                            else{
                                                echo "No Vendors selected.";
                                            }
                                                ?>
                                                <!-- <div id="chk_list" class="row">

                                            </div> -->
                                            <br>
                                            <br>

                                            <h4 class="card-title">Select Vendors</h4>
                                            <form action="/path/to/your/script.php" method="POST" id="vendor">
                                                <div class="table-responsive">
                                                    <table id="memListTable" class="table table-striped table-bordered"
                                                        cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th>Name</th>
                                                                <th>Location</th>
                                                                <th>Product</th>
                                                                <th>Person</th>
                                                                <th>Email</th>
                                                                <th>Mobile1</th>
                                                                <th>Mobile2</th>
                                                                <th>Mobile3</th>
                                                                <th>Mobile4</th>
                                                                <th>Whatsapp</th>

                                                                <!-- th><button onclick="" class="btn btn-sm btn-success "><i class="fa fa-check m-r-5"></i>View</button></th> -->
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <!-- <div id="chk_list" class="row">

                                            </div> -->



                                                <input type="hidden" name="id" id="e_id" value="<?= $enqueiry_id ?>">
                                                <input type="hidden" name="vendors" id="str">
                                                <button class="btn btn-success">
                                                    Submit
                                                </button>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php include "footer.php"; ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- customizer Panel -->
    <!-- ============================================================== -->
    <aside class="customizer">
        <a href="javascript:void(0)" class="service-panel-toggle"><i class="fa fa-spin fa-cog"></i></a>
        <div class="customizer-body">
            <ul class="nav customizer-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
                        aria-controls="pills-home" aria-selected="true"><i class="mdi mdi-wrench font-20"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#chat" role="tab"
                        aria-controls="chat" aria-selected="false"><i class="mdi mdi-message-reply font-20"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab"
                        aria-controls="pills-contact" aria-selected="false"><i
                            class="mdi mdi-star-circle font-20"></i></a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <!-- Tab 1 -->
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="p-15 border-bottom">
                        <!-- Sidebar -->
                        <h5 class="font-medium m-b-10 m-t-10">Layout Settings</h5>
                        <div class="custom-control custom-checkbox m-t-10">
                            <input type="checkbox" class="custom-control-input" name="theme-view" id="theme-view">
                            <label class="custom-control-label" for="theme-view">Dark Theme</label>
                        </div>
                        <div class="custom-control custom-checkbox m-t-10">
                            <input type="checkbox" class="custom-control-input sidebartoggler" name="collapssidebar"
                                id="collapssidebar">
                            <label class="custom-control-label" for="collapssidebar">Collapse Sidebar</label>
                        </div>
                        <div class="custom-control custom-checkbox m-t-10">
                            <input type="checkbox" class="custom-control-input" name="sidebar-position"
                                id="sidebar-position">
                            <label class="custom-control-label" for="sidebar-position">Fixed Sidebar</label>
                        </div>
                        <div class="custom-control custom-checkbox m-t-10">
                            <input type="checkbox" class="custom-control-input" name="header-position"
                                id="header-position">
                            <label class="custom-control-label" for="header-position">Fixed Header</label>
                        </div>
                        <div class="custom-control custom-checkbox m-t-10">
                            <input type="checkbox" class="custom-control-input" name="boxed-layout" id="boxed-layout">
                            <label class="custom-control-label" for="boxed-layout">Boxed Layout</label>
                        </div>
                    </div>
                    <div class="p-15 border-bottom">
                        <!-- Logo BG -->
                        <h5 class="font-medium m-b-10 m-t-10">Logo Backgrounds</h5>
                        <ul class="theme-color">
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-logobg="skin1"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-logobg="skin2"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-logobg="skin3"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-logobg="skin4"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-logobg="skin5"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-logobg="skin6"></a></li>
                        </ul>
                        <!-- Logo BG -->
                    </div>
                    <div class="p-15 border-bottom">
                        <!-- Navbar BG -->
                        <h5 class="font-medium m-b-10 m-t-10">Navbar Backgrounds</h5>
                        <ul class="theme-color">
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-navbarbg="skin1"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-navbarbg="skin2"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-navbarbg="skin3"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-navbarbg="skin4"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-navbarbg="skin5"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-navbarbg="skin6"></a></li>
                        </ul>
                        <!-- Navbar BG -->
                    </div>
                    <div class="p-15 border-bottom">
                        <!-- Logo BG -->
                        <h5 class="font-medium m-b-10 m-t-10">Sidebar Backgrounds</h5>
                        <ul class="theme-color">
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-sidebarbg="skin1"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-sidebarbg="skin2"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-sidebarbg="skin3"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-sidebarbg="skin4"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-sidebarbg="skin5"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-sidebarbg="skin6"></a></li>
                        </ul>
                        <!-- Logo BG -->
                    </div>
                </div>
                <!-- End Tab 1 -->
                <!-- Tab 2 -->
                <div class="tab-pane fade" id="chat" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <ul class="mailbox list-style-none m-t-20">
                        <li>
                            <div class="message-center chat-scroll">
                                <a href="javascript:void(0)" class="message-item" id='chat_user_1' data-user-id='1'>
                                    <span class="user-img"> <img src="assets/images/users/1.jpg" alt="user"
                                            class="rounded-circle"> <span
                                            class="profile-status online pull-right"></span> </span>
                                    <span class="mail-contnet">
                                        <h5 class="message-title">Pavan kumar</h5> <span class="mail-desc">Just see the
                                            my admin!</span> <span class="time">9:30 AM</span>
                                    </span>
                                </a>
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_2' data-user-id='2'>
                                    <span class="user-img"> <img src="assets/images/users/2.jpg" alt="user"
                                            class="rounded-circle"> <span class="profile-status busy pull-right"></span>
                                    </span>
                                    <span class="mail-contnet">
                                        <h5 class="message-title">Sonu Nigam</h5> <span class="mail-desc">I've sung a
                                            song! See you at</span> <span class="time">9:10 AM</span>
                                    </span>
                                </a>
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_3' data-user-id='3'>
                                    <span class="user-img"> <img src="assets/images/users/3.jpg" alt="user"
                                            class="rounded-circle"> <span class="profile-status away pull-right"></span>
                                    </span>
                                    <span class="mail-contnet">
                                        <h5 class="message-title">Arijit Sinh</h5> <span class="mail-desc">I am a
                                            singer!</span> <span class="time">9:08 AM</span>
                                    </span>
                                </a>
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_4' data-user-id='4'>
                                    <span class="user-img"> <img src="assets/images/users/4.jpg" alt="user"
                                            class="rounded-circle"> <span
                                            class="profile-status offline pull-right"></span> </span>
                                    <span class="mail-contnet">
                                        <h5 class="message-title">Nirav Joshi</h5> <span class="mail-desc">Just see the
                                            my admin!</span> <span class="time">9:02 AM</span>
                                    </span>
                                </a>
                                <!-- Message -->
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_5' data-user-id='5'>
                                    <span class="user-img"> <img src="assets/images/users/5.jpg" alt="user"
                                            class="rounded-circle"> <span
                                            class="profile-status offline pull-right"></span> </span>
                                    <span class="mail-contnet">
                                        <h5 class="message-title">Sunil Joshi</h5> <span class="mail-desc">Just see the
                                            my admin!</span> <span class="time">9:02 AM</span>
                                    </span>
                                </a>
                                <!-- Message -->
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_6' data-user-id='6'>
                                    <span class="user-img"> <img src="assets/images/users/6.jpg" alt="user"
                                            class="rounded-circle"> <span
                                            class="profile-status offline pull-right"></span> </span>
                                    <span class="mail-contnet">
                                        <h5 class="message-title">Akshay Kumar</h5> <span class="mail-desc">Just see the
                                            my admin!</span> <span class="time">9:02 AM</span>
                                    </span>
                                </a>
                                <!-- Message -->
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_7' data-user-id='7'>
                                    <span class="user-img"> <img src="assets/images/users/7.jpg" alt="user"
                                            class="rounded-circle"> <span
                                            class="profile-status offline pull-right"></span> </span>
                                    <span class="mail-contnet">
                                        <h5 class="message-title">Pavan kumar</h5> <span class="mail-desc">Just see the
                                            my admin!</span> <span class="time">9:02 AM</span>
                                    </span>
                                </a>
                                <!-- Message -->
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_8' data-user-id='8'>
                                    <span class="user-img"> <img src="assets/images/users/8.jpg" alt="user"
                                            class="rounded-circle"> <span
                                            class="profile-status offline pull-right"></span> </span>
                                    <span class="mail-contnet">
                                        <h5 class="message-title">Varun Dhavan</h5> <span class="mail-desc">Just see the
                                            my admin!</span> <span class="time">9:02 AM</span>
                                    </span>
                                </a>
                                <!-- Message -->
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- End Tab 2 -->
                <!-- Tab 3 -->
                <div class="tab-pane fade p-15" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h6 class="m-t-20 m-b-20">Activity Timeline</h6>
                    <div class="steamline">
                        <div class="sl-item">
                            <div class="sl-left bg-success"> <i class="ti-user"></i></div>
                            <div class="sl-right">
                                <div class="font-medium">Meeting today <span class="sl-date"> 5pm</span></div>
                                <div class="desc">you can write anything </div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left bg-info"><i class="fas fa-image"></i></div>
                            <div class="sl-right">
                                <div class="font-medium">Send documents to Clark</div>
                                <div class="desc">Lorem Ipsum is simply </div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left"> <img class="rounded-circle" alt="user"
                                    src="assets/images/users/2.jpg"> </div>
                            <div class="sl-right">
                                <div class="font-medium">Go to the Doctor <span class="sl-date">5 minutes ago</span>
                                </div>
                                <div class="desc">Contrary to popular belief</div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left"> <img class="rounded-circle" alt="user"
                                    src="assets/images/users/1.jpg"> </div>
                            <div class="sl-right">
                                <div><a href="javascript:void(0)">Stephen</a> <span class="sl-date">5 minutes ago</span>
                                </div>
                                <div class="desc">Approve meeting with tiger</div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left bg-primary"> <i class="ti-user"></i></div>
                            <div class="sl-right">
                                <div class="font-medium">Meeting today <span class="sl-date"> 5pm</span></div>
                                <div class="desc">you can write anything </div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left bg-info"><i class="fas fa-image"></i></div>
                            <div class="sl-right">
                                <div class="font-medium">Send documents to Clark</div>
                                <div class="desc">Lorem Ipsum is simply </div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left"> <img class="rounded-circle" alt="user"
                                    src="assets/images/users/4.jpg"> </div>
                            <div class="sl-right">
                                <div class="font-medium">Go to the Doctor <span class="sl-date">5 minutes ago</span>
                                </div>
                                <div class="desc">Contrary to popular belief</div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left"> <img class="rounded-circle" alt="user"
                                    src="assets/images/users/6.jpg"> </div>
                            <div class="sl-right">
                                <div><a href="javascript:void(0)">Stephen</a> <span class="sl-date">5 minutes ago</span>
                                </div>
                                <div class="desc">Approve meeting with tiger</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Tab 3 -->
            </div>
        </div>
    </aside>
    <div class="chat-windows"></div>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->


    <script src="assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="dist/js/app.min.js"></script>
    <script src="dist/js/app.init.light-sidebar.js"></script>
    <script src="dist/js/app-style-switcher.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <script src="assets/libs/dropzone/dist/min/dropzone.min.js"></script>

    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <script src="sweetalert/sweetalert.min.js"></script>
    <script src="assets/libs/select2/dist/js/select2.full.min.js"></script>
    <script src="assets/libs/select2/dist/js/select2.min.js"></script>
    <script src="dist/js/pages/forms/select2/select2.init.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js">
    </script>
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css"
        rel="stylesheet" />
    <script type="text/javascript"
        src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>
    <script>
    $(document).ready(function() {
        var table = $('#memListTable').DataTable({
            lengthChange: false,
            ajax: {
                url: "api/web/get-vendors.php",
                dataSrc: "", 
                data: {
                    id: "<?= $_POST['enqueiry_id'] ?>"
                },
            },
            columns: [
                {
                    data: "id"
                },
                {
                    data: "name"
                },
                {
                    data: "location"
                }, {
                    data: "product"
                }, {
                    data: "person"
                }, {
                    data: "email"
                }, {
                    data: "mobile1"
                }, {
                    data: "mobile2"
                }, {
                    data: "mobile3"
                }, {
                    data: "mobile4"
                }, {
                    data: "whatsapp"
                },

                //    {
                // 	"render": function ( data, type, row) {
                // return '<a class="btn btn-sm btn-success" href="view_enquiry.php?id=' + row.ce_id + '"><i class="fa fa-search m-r-5"></i>Details</a>';       	         
                //     }
                // }

            ],
            'columnDefs': [{
                'targets': 0,
                'checkboxes': {
                    'selectRow': true
                }
            }],
            'select': {
                'style': 'multi'
            }
        });



        // var table = $('#memListTable').DataTable({
        //     "processing": true,
        //     "serverSide": true,
        //     "ajax": "api/web/get-vendors.php",
        //     'columnDefs': [{
        //         'targets': 0,
        //         'checkboxes': {
        //             'selectRow': true
        //         }
        //     }],
        //     'select': {
        //         'style': 'multi'
        //     },
        //     'order': [
        //         [1, 'asc']
        //     ]
        // });

        $('#vendor').on('submit', function(e) {
            e.preventDefault();
            var id = $('#e_id').val();

            var form = this;
            var rows_selected = table.column(0).checkboxes.selected();

            // Iterate over all selected checkboxes

            $('#str').val(rows_selected.join(":"));
            var vendors = document.getElementById('str').value;
            console.log(vendors);
            if (vendors == '') {
                e.preventDefault();
                swal({
                    title: "Error",
                    text: "Please Select Vendors",
                    icon: "warning"
                });

            } else {
                e.preventDefault();
                e.stopPropagation();
                let form = new FormData($('form')[0]);

                $.ajax({
                    method: 'POST',
                    url: 'api/web/save-vendor-to-enquery.php',
                    data: form,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if (jQuery.trim(data) === "Success") {
                            swal({
                                title: "Success",
                                text: "Data Added.",
                                icon: "success"
                            });

                            document.getElementById("vendor").reset();
                            top.location.href = 'view_enquiry.php?id=' + id;
                        }
                    },
                    error: function(error) {
                        swal({
                            title: "Error",
                            text: "Error AJAX not working: " + error,
                            icon: "error"
                        });
                    }
                });

            }

            // Remove added elements
            $('input[name="id\[\]"]', form).remove();



        });
    });
    </script>
    <!-- <script>
        $(document).ready(function() {
            $.ajax({
                url: 'api/web/vendors-checkbox.php',
                type: 'GET',
                data: {
                    option: name
                },
                success: function(data) {
                    console.log(data);
                    document.getElementById('chk_list').innerHTML = data;
                }
            });
        })
    </script> -->
    <script>
    // document.querySelector("button[type=submit]").addEventListener("click", function(e) {
    //     e.preventDefault();
    //     var sThisVal = '';
    //     var final = '';
    //     $('input:checkbox.vendor').each(function() {
    //         if (this.checked) {
    //             var val = $(this).val();
    //             sThisVal += val + ':';
    //             console.log(sThisVal)
    //         }
    //     });
    //     if (sThisVal != '') {
    //         final = sThisVal.slice(0, -1)
    //     }

    //     console.log(final);

    //     var id = $('#e_id').val();

    //     document.getElementById('str').value = final;


    // });
    </script>
</body>

</html>
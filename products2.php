<?php
session_start();
if (!isset($_SESSION['logged_in'])) {
    header("Location:index.php");
}
include("api/web/dbconfig.php");
include("libs/phpexcel/PHPExcel/IOFactory.php");
require 'libs/phpspreadsheet/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;

date_default_timezone_set('Asia/Calcutta');


?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>All Vendors</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/xtremeadmin/" />
    <!-- Custom CSS -->
    <link href="dist/css/style.min.css" rel="stylesheet">
    <link href="sweetalert/sweetalert.css" rel="stylesheet">
    <link href="assets/libs/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
    <!-- <link href="sweetalert/sweetalert.css" rel="stylesheet"> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <style>
        img.product-image {
            min-height: 250px;
        }
    </style>
</head>

<body>

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include "header.php"; ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php include "sidebar.php"; ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Eco Products</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Products</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-7 align-self-center">

                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <div id="frm">
                            <form method="POST">
                                <div class="filter" style="padding-left: 40px; padding-bottom: 30px;">
                                    <h4>Search Product</h4>
                                    <input type="text" name="search_text" id="search_text" placeholder="Search by Product Name" class="form-control" />
                                </div>
                                <div class="filter" style="padding-left: 40px; padding-bottom: 30px;">
                                    <h4>Short By Name</h4>
                                    <select name="short" class="short">
                                        <option value="select" selected>--Select--</option>
                                        <option value="atoz">Short By A-Z</option>
                                        <option value="ztoa">Short By Z-A</option>
                                    </select>
                                </div>
                                <ul class="filter">
                                    <h4>Filter By Materials</h4>
                                    <?php
                                    $sql = "select * from materials";
                                    $result = $connection->query($sql);
                                    if ($result->num_rows > 0) {
                                        while ($row = $result->fetch_assoc()) {
                                    ?>
                                            <input type="checkbox" name="mat_ids[]" value="<?php echo $row['mat_id']; ?>" id="<?php echo $row['mat_id']; ?>" class="mat_ids" /> <label><?php echo $row['mat_name']; ?></label><br>
                                    <?php
                                        }
                                    }
                                    ?>
                                </ul>

                                <ul class="filter">
                                    <h4>Filter By Product Type</h4>
                                    <?php

                                    $sql = "select * from producttype";
                                    $result = $connection->query($sql);
                                    if ($result->num_rows > 0) {
                                        while ($row = $result->fetch_assoc()) {
                                    ?>
                                            <input type="checkbox" name="prd_ids[]" value="<?php echo $row['prdtype_id']; ?>" id="<?php echo $row['prdtype_id']; ?>" class="prd_ids" /> <label><?php echo $row['prd_type_name']; ?></label><br>
                                    <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </form>
                        </div>

                    </div>
                    <!-- ============================================================== -->
                    <!-- Start Page Content -->
                    <!-- ============================================================== -->
                    <div class="col-md-9">
                        <div class="row el-element-overlay" id="result">
                            <?php
                            $sql = "select * from products order by prod_name ASC";
                            $result = $connection->query($sql);

                            if ($result->num_rows > 0) {
                                while ($row = $result->fetch_assoc()) {
                            ?>
                                    <div class="col-lg-4 col-md-12">
                                        <div class="card">
                                            <div class="el-card-item">
                                                <div class="el-card-avatar el-overlay-1">
                                                    <?php
                                                    if ($row['drawings'] != '') {
                                                        $imageNames = explode(":", $row['drawings']);
                                                        $abc = $imageNames[0];
                                                    ?>
                                                        <img class="product-image" src="uploads/products/<?= $abc ?>" alt="">
                                                        <div class="el-overlay">
                                                            <ul class="list-style-none el-info">
                                                                <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="uploads/products/<?= $abc ?>"><i class="icon-magnifier"></i></a></li>
                                                                <li class="el-item"><a class="btn default btn-outline el-link" href="product-single.php?id=<?= $row['prod_id'] ?>"><i class="icon-link"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    <?php
                                                    } else {
                                                    ?>
                                                        <img class="product-image" src="uploads/event_files/20191022071419525700.jpg" alt="">\
                                                        <div class="el-overlay">
                                                            <ul class="list-style-none el-info">
                                                                <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="assets/images/gallery/chair.jpg"><i class="icon-magnifier"></i></a></li>
                                                                <li class="el-item"><a class="btn default btn-outline el-link" href="product-single.php?id=<?= $row['prod_id'] ?>"><i class="icon-link"></i></a></li>
                                                            </ul>
                                                        </div>

                                                    <?php
                                                    }

                                                    ?>


                                                </div>
                                                <div class="d-flex no-block align-items-center">
                                                    <div class="m-l-15">
                                                        <h4 class="m-b-0"><?= $row['prod_name'] ?></h4>
                                                        <span class="text-muted">globe type chair for rest</span>
                                                    </div>
                                                    <div class="ml-auto m-r-15">
                                                        <button type="button" class="btn btn-dark btn-circle">$15</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Xtreme admin. Designed and Developed by <a href="https://wrappixel.com/">WrapPixel</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- customizer Panel -->
    <!-- ============================================================== -->
    <aside class="customizer">

        <div class="customizer-body">
            <ul class="nav customizer-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><i class="mdi mdi-wrench font-20"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#chat" role="tab" aria-controls="chat" aria-selected="false"><i class="mdi mdi-message-reply font-20"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false"><i class="mdi mdi-star-circle font-20"></i></a>
                </li>
            </ul>

        </div>
    </aside>
    <div class="chat-windows"></div>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->


    <script src="assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="dist/js/app.min.js"></script>
    <script src="dist/js/app.init.light-sidebar.js"></script>
    <script src="dist/js/app-style-switcher.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <script src="assets/libs/dropzone/dist/min/dropzone.min.js"></script>

    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <script src="sweetalert/sweetalert.min.js"></script>

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function() {
                    $('.mat_ids').on('change', function() { //on checkboxes check
                        var mat_ids = $('.mat_ids:checked').serialize();
                        var prd_ids = $('.prd_ids:checked').serialize();
                        var short = $('.short').val();
                        var query = $('#search_text').val();
                        if (prd_ids != '' || mat_ids != '') {
                            $.ajax({
                                type: "POST",
                                cache: false,
                                url: "api/web/filter.php",
                                data: {
                                    mat_ids: mat_ids,
                                    prd_ids: prd_ids,
                                    short: short,
                                    query: query
                                },
                                success: function(response) {
                                    // document.getElementById('getdata').style.display = "block";
                                    document.getElementById("result").innerHTML = response;
                                    // $('#result').hide();
                                }
                            });
                        } else {
                            $.ajax({
                                type: "POST",
                                cache: false,
                                url: "api/web/filter.php",
                                data: {
                                    mat_ids: '',
                                    prd_ids: '',
                                    short: short,
                                    query: query

                                },
                                success: function(response) {
                                    // document.getElementById('getdata').style.display = "block";
                                    document.getElementById("result").innerHTML = response;
                                    // $('#result').hide();
                                }
                            });
                        }
                    });

                    $('.prd_ids').on('change', function() { //on checkboxes check
                        var mat_ids = $('.mat_ids:checked').serialize();
                        var prd_ids = $('.prd_ids:checked').serialize();
                        var short = $('.short').val();
                        var query = $('#search_text').val();
                        if (prd_ids != '' || mat_ids != '') {
                            $.ajax({
                                type: "POST",
                                cache: false,
                                url: "api/web/filter.php",
                                data: {
                                    mat_ids: mat_ids,
                                    prd_ids: prd_ids,
                                    short: short,
                                    query: query
                                },
                                success: function(response) {
                                    // document.getElementById('getdata').style.display = "block";
                                    document.getElementById("result").innerHTML = response;
                                    // $('#result').hide();
                                }
                            });
                        } else {
                            $.ajax({
                                type: "POST",
                                cache: false,
                                url: "api/web/filter.php",
                                data: {
                                    mat_ids: '',
                                    prd_ids: '',
                                    short: short,
                                    query: query

                                },
                                success: function(response) {
                                    // document.getElementById('getdata').style.display = "block";
                                    document.getElementById("result").innerHTML = response;
                                    // $('#result').hide();
                                }
                            });
                        }
                    });

                    $(function() {
                        $(".short").change(function() {
                            var short = $('option:selected', this).val();
                            var mat_ids = $('.mat_ids:checked').serialize();
                            var prd_ids = $('.prd_ids:checked').serialize();
                            var query = $('#search_text').val();
                            if (short) {
                                $.ajax({
                                    type: "POST",
                                    cache: false,
                                    url: "api/web/filter.php",
                                    data: {
                                        mat_ids: mat_ids,
                                        prd_ids: prd_ids,
                                        short: short,
                                        query: query
                                    },
                                    success: function(response) {
                                        // document.getElementById('getdata').style.display = "block";
                                        document.getElementById("result").innerHTML = response;
                                        // $('#result').hide();
                                    }
                                });
                            } else {
                                $.ajax({
                                    type: "POST",
                                    cache: false,
                                    url: "api/web/filter.php",
                                    data: {
                                        mat_ids: '',
                                        prd_ids: '',
                                        short: 'atoz',
                                        query: query

                                    },
                                    success: function(response) {
                                        // document.getElementById('getdata').style.display = "block";
                                        document.getElementById("result").innerHTML = response;
                                        // $('#result').hide();
                                    }
                                });
                            }
                        });
                    });

                    function load_data(query) {
                        var short = $('option:selected', this).val();
                        var mat_ids = $('.mat_ids:checked').serialize();
                        var prd_ids = $('.prd_ids:checked').serialize();
                        console.log(short);
                        if(short == undefined){
                            short = 'atoz';
                        }
                        $.ajax({
                            url: "api/web/filter.php",
                            method: "POST",
                            data: {
                                mat_ids: mat_ids,
                                prd_ids: prd_ids,
                                short: short,
                                query: query
                            },
                            success: function(response) {
                                document.getElementById("result").innerHTML = response;
                            }
                        });
                    }
                    $('#search_text').keyup(function() {
                            var search = $(this).val();
                            var short = $('option:selected', this).val();
                            var mat_ids = $('.mat_ids:checked').serialize();
                            var prd_ids = $('.prd_ids:checked').serialize();
                            if (search != '') {
                                load_data(search);
                            } else {
                                $.ajax({
                                    type: "POST",
                                    cache: false,
                                    url: "api/web/filter.php",
                                    data: {
                                        mat_ids: '',
                                        prd_ids: '',
                                        short: 'atoz',
                                        query: ''

                                    },
                                    success: function(response) {
                                        // document.getElementById('getdata').style.display = "block";
                                        document.getElementById("result").innerHTML = response;
                                        // $('#result').hide();
                                    }
                                });
                            }

                    });
        })
    </script>
</body>

</html>
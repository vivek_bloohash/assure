<?php 
include("dbconfig.php");
 
if ($connection->connect_error) { 
    die("Connection failed: " . $connection->connect_error); 
} 
 
// Get search term 
$searchTerm = $_GET['term']; 
 
// Fetch matched data from the database 
$query = $connection->query("SELECT * FROM materials WHERE mat_name LIKE '%".$searchTerm."%'  ORDER BY mat_name ASC"); 
 
// Generate array with skills data 
$skillData = array(); 
if($query->num_rows > 0){ 
    while($row = $query->fetch_assoc()){ 
        $data['id'] = $row['mat_id']; 
        $data['value'] = $row['mat_name']; 
        array_push($skillData, $data); 
    } 
} 

echo json_encode($skillData); 
?>
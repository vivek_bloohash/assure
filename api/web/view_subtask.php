<?php

include("dbconfig.php");

$response = array();
if ($_SERVER['REQUEST_METHOD'] === 'GET') {

  $_ide = mysqli_real_escape_string($connection, $_GET["id"]);
  $query = "SELECT subtasks.id as id, subtasks.subtask_name as subtask_name, subtasks.subtask_leader as subtask_leader, subtasks.start_date as start_date, subtasks.end_date as end_date, subtasks.actual_start as actual_start, subtasks.actual_finish as actual_finish, subtasks.status as status, subtasks.task_id as task_id, tasks.task_name as task_name, subtasks.subtask_leader as subtask_leader FROM `subtasks` LEFT JOIN tasks ON subtasks.task_id = tasks.id where subtasks.id=$_ide";
  $result = mysqli_query($connection, $query);
  // where ce_id='$_ide
  header('Content-Type: application/json');
  while ($row = mysqli_fetch_assoc($result)) {
    array_push($response, $row);
  }
  echo json_encode($response); // Parse to JSON and print.

}

<?php
include("dbconfig.php");
$short = $_POST['short'];
$search = mysqli_real_escape_string($connection, $_POST["query"]);

if($search != ''){
    if ($_POST['mat_ids'] != '' && $_POST['prd_ids'] != '') {
        $mat_ids = array();
        parse_str($_POST['mat_ids'], $mat_ids);
        parse_str($_POST['prd_ids'], $prd_ids);
    
        foreach ($mat_ids as $m_ids) {
            $m_ids;
        }
    
        foreach ($prd_ids as $p_ids) {
            $p_ids;
        }
        $material_id = implode("','", $m_ids);
        $product_type_id = implode("','", $p_ids);
        echo "<br>";
        if($short == 'ztoa'){
            $sql = "SELECT * FROM products WHERE mat_id IN ('$material_id') or prdtype_id in ('$product_type_id') and prod_name LIKE '%".$search."%' order by prod_name DESC";
            
        }
        else{
            $sql = "SELECT * FROM products WHERE mat_id IN ('$material_id') or prdtype_id in ('$product_type_id') and prod_name LIKE '%".$search."%' order by prod_name ASC";
        }
        // echo $sql;
        $result = $connection->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
    ?>
                <div class="col-lg-4 col-md-12">
                    <div class="card">
                        <div class="el-card-item">
                            <div class="el-card-avatar el-overlay-1">
                                <?php
                                if ($row['drawings'] != '') {
                                    $imageNames = explode(":", $row['drawings']);
                                    $abc = $imageNames[0];
                                ?>
                                    <img class="product-image" src="uploads/products/<?= $abc ?>" alt="">
                                    <div class="el-overlay">
                                        <ul class="list-style-none el-info">
                                            <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="uploads/products/<?= $abc ?>"><i class="icon-magnifier"></i></a></li>
                                            <li class="el-item"><a class="btn default btn-outline el-link" href="product-single.php?id=<?= $row['prod_id'] ?>"><i class="icon-link"></i></a></li>
                                        </ul>
                                    </div>
                                <?php
                                } else {
                                ?>
                                    <img class="product-image" src="uploads/event_files/20191022071419525700.jpg" alt="">\
                                    <div class="el-overlay">
                                        <ul class="list-style-none el-info">
                                            <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="assets/images/gallery/chair.jpg"><i class="icon-magnifier"></i></a></li>
                                            <li class="el-item"><a class="btn default btn-outline el-link" href="product-single.php?id=<?= $row['prod_id'] ?>"><i class="icon-link"></i></a></li>
                                        </ul>
                                    </div>
    
                                <?php
                                }
    
                                ?>
    
    
                            </div>
                            <div class="d-flex no-block align-items-center">
                                <div class="m-l-15">
                                    <h4 class="m-b-0"><?= $row['prod_name'] ?></h4>
                                    <span class="text-muted">globe type chair for rest</span>
                                </div>
                                <div class="ml-auto m-r-15">
                                    <button type="button" class="btn btn-dark btn-circle">$15</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
        }
    } elseif ($_POST['mat_ids'] != '' && $_POST['prd_ids'] == '') {
        $mat_ids = array();
        parse_str($_POST['mat_ids'], $mat_ids);
    
        foreach ($mat_ids as $m_ids) {
            $m_ids;
        }
    
        $material_id = implode("','", $m_ids);
    
        echo "<br>";
        if($short == 'ztoa'){
            $sql = "SELECT * FROM products WHERE mat_id IN ('$material_id') and prod_name LIKE '%".$search."%' order by prod_name DESC";
        }
        else{
            $sql = "SELECT * FROM products WHERE mat_id IN ('$material_id') and prod_name LIKE '%".$search."%' order by prod_name ASC";
        }
        $result = $connection->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
            ?>
                <div class="col-lg-4 col-md-12">
                    <div class="card">
                        <div class="el-card-item">
                            <div class="el-card-avatar el-overlay-1">
                                <?php
                                if ($row['drawings'] != '') {
                                    $imageNames = explode(":", $row['drawings']);
                                    $abc = $imageNames[0];
                                ?>
                                    <img class="product-image" src="uploads/products/<?= $abc ?>" alt="">
                                    <div class="el-overlay">
                                        <ul class="list-style-none el-info">
                                            <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="uploads/products/<?= $abc ?>"><i class="icon-magnifier"></i></a></li>
                                            <li class="el-item"><a class="btn default btn-outline el-link" href="product-single.php?id=<?= $row['prod_id'] ?>"><i class="icon-link"></i></a></li>
                                        </ul>
                                    </div>
                                <?php
                                } else {
                                ?>
                                    <img class="product-image" src="uploads/event_files/20191022071419525700.jpg" alt="">\
                                    <div class="el-overlay">
                                        <ul class="list-style-none el-info">
                                            <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="assets/images/gallery/chair.jpg"><i class="icon-magnifier"></i></a></li>
                                            <li class="el-item"><a class="btn default btn-outline el-link" href="product-single.php?id=<?= $row['prod_id'] ?>"><i class="icon-link"></i></a></li>
                                        </ul>
                                    </div>
    
                                <?php
                                }
    
                                ?>
    
    
                            </div>
                            <div class="d-flex no-block align-items-center">
                                <div class="m-l-15">
                                    <h4 class="m-b-0"><?= $row['prod_name'] ?></h4>
                                    <span class="text-muted">globe type chair for rest</span>
                                </div>
                                <div class="ml-auto m-r-15">
                                    <button type="button" class="btn btn-dark btn-circle">$15</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
        }
    } elseif ($_POST['mat_ids'] == '' && $_POST['prd_ids'] != '') {
        $prd_ids = array();
        parse_str($_POST['prd_ids'], $prd_ids);
    
        foreach ($prd_ids as $p_ids) {
            $p_ids;
        }
    
        $prdtype_id = implode("','", $p_ids);
    
        echo "<br>";
        if($short == 'ztoa'){
            $sql = "SELECT * FROM products WHERE prdtype_id IN ('$prdtype_id') and prod_name LIKE '%".$search."%' order by prod_name DESC";
            
        }
        else{
            $sql = "SELECT * FROM products WHERE prdtype_id IN ('$prdtype_id') and prod_name LIKE '%".$search."%' order by prod_name ASC";
        }
        $result = $connection->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
            ?>
                <div class="col-lg-4 col-md-12">
                    <div class="card">
                        <div class="el-card-item">
                            <div class="el-card-avatar el-overlay-1">
                                <?php
                                if ($row['drawings'] != '') {
                                    $imageNames = explode(":", $row['drawings']);
                                    $abc = $imageNames[0];
                                ?>
                                    <img class="product-image" src="uploads/products/<?= $abc ?>" alt="">
                                    <div class="el-overlay">
                                        <ul class="list-style-none el-info">
                                            <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="uploads/products/<?= $abc ?>"><i class="icon-magnifier"></i></a></li>
                                            <li class="el-item"><a class="btn default btn-outline el-link" href="product-single.php?id=<?= $row['prod_id'] ?>"><i class="icon-link"></i></a></li>
                                        </ul>
                                    </div>
                                <?php
                                } else {
                                ?>
                                    <img class="product-image" src="uploads/event_files/20191022071419525700.jpg" alt="">\
                                    <div class="el-overlay">
                                        <ul class="list-style-none el-info">
                                            <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="assets/images/gallery/chair.jpg"><i class="icon-magnifier"></i></a></li>
                                            <li class="el-item"><a class="btn default btn-outline el-link" href="product-single.php?id=<?= $row['prod_id'] ?>"><i class="icon-link"></i></a></li>
                                        </ul>
                                    </div>
    
                                <?php
                                }
    
                                ?>
    
    
                            </div>
                            <div class="d-flex no-block align-items-center">
                                <div class="m-l-15">
                                    <h4 class="m-b-0"><?= $row['prod_name'] ?></h4>
                                    <span class="text-muted">globe type chair for rest</span>
                                </div>
                                <div class="ml-auto m-r-15">
                                    <button type="button" class="btn btn-dark btn-circle">$15</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
        }
    } else {
    if($short == 'ztoa'){
            $sql = "SELECT * FROM products where prod_name LIKE '%".$search."%' order by prod_name DESC";
        }
        else{
            $sql = "SELECT * FROM products where prod_name LIKE '%".$search."%' order by prod_name ASC";
        }
        $result = $connection->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
            ?>
                <div class="col-lg-4 col-md-12">
                    <div class="card">
                        <div class="el-card-item">
                            <div class="el-card-avatar el-overlay-1">
                                <?php
                                if ($row['drawings'] != '') {
                                    $imageNames = explode(":", $row['drawings']);
                                    $abc = $imageNames[0];
                                ?>
                                    <img class="product-image" src="uploads/products/<?= $abc ?>" alt="">
                                    <div class="el-overlay">
                                        <ul class="list-style-none el-info">
                                            <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="uploads/products/<?= $abc ?>"><i class="icon-magnifier"></i></a></li>
                                            <li class="el-item"><a class="btn default btn-outline el-link" href="product-single.php?id=<?= $row['prod_id'] ?>"><i class="icon-link"></i></a></li>
                                        </ul>
                                    </div>
                                <?php
                                } else {
                                ?>
                                    <img class="product-image" src="uploads/event_files/20191022071419525700.jpg" alt="">\
                                    <div class="el-overlay">
                                        <ul class="list-style-none el-info">
                                            <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="assets/images/gallery/chair.jpg"><i class="icon-magnifier"></i></a></li>
                                            <li class="el-item"><a class="btn default btn-outline el-link" href="product-single.php?id=<?= $row['prod_id'] ?>"><i class="icon-link"></i></a></li>
                                        </ul>
                                    </div>
    
                                <?php
                                }
                                ?>
                            </div>
                            <div class="d-flex no-block align-items-center">
                                <div class="m-l-15">
                                    <h4 class="m-b-0"><?= $row['prod_name'] ?></h4>
                                    <span class="text-muted">globe type chair for rest</span>
                                </div>
                                <div class="ml-auto m-r-15">
                                    <button type="button" class="btn btn-dark btn-circle">$15</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    <?php
            }
        }
    }
}
else{
    if ($_POST['mat_ids'] != '' && $_POST['prd_ids'] != '') {
        $mat_ids = array();
        parse_str($_POST['mat_ids'], $mat_ids);
        parse_str($_POST['prd_ids'], $prd_ids);
    
        foreach ($mat_ids as $m_ids) {
            $m_ids;
        }
    
        foreach ($prd_ids as $p_ids) {
            $p_ids;
        }
        $material_id = implode("','", $m_ids);
        $product_type_id = implode("','", $p_ids);
        echo "<br>";
        if($short == 'ztoa'){
            $sql = "SELECT * FROM products WHERE mat_id IN ('$material_id') or prdtype_id in ('$product_type_id') order by prod_name DESC";
            
        }
        else{
            $sql = "SELECT * FROM products WHERE mat_id IN ('$material_id') or prdtype_id in ('$product_type_id') order by prod_name ASC";
        }
        // echo $sql;
        $result = $connection->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
    ?>
                <div class="col-lg-4 col-md-12">
                    <div class="card">
                        <div class="el-card-item">
                            <div class="el-card-avatar el-overlay-1">
                                <?php
                                if ($row['drawings'] != '') {
                                    $imageNames = explode(":", $row['drawings']);
                                    $abc = $imageNames[0];
                                ?>
                                    <img class="product-image" src="uploads/products/<?= $abc ?>" alt="">
                                    <div class="el-overlay">
                                        <ul class="list-style-none el-info">
                                            <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="uploads/products/<?= $abc ?>"><i class="icon-magnifier"></i></a></li>
                                            <li class="el-item"><a class="btn default btn-outline el-link" href="product-single.php?id=<?= $row['prod_id'] ?>"><i class="icon-link"></i></a></li>
                                        </ul>
                                    </div>
                                <?php
                                } else {
                                ?>
                                    <img class="product-image" src="uploads/event_files/20191022071419525700.jpg" alt="">\
                                    <div class="el-overlay">
                                        <ul class="list-style-none el-info">
                                            <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="assets/images/gallery/chair.jpg"><i class="icon-magnifier"></i></a></li>
                                            <li class="el-item"><a class="btn default btn-outline el-link" href="product-single.php?id=<?= $row['prod_id'] ?>"><i class="icon-link"></i></a></li>
                                        </ul>
                                    </div>
    
                                <?php
                                }
    
                                ?>
    
    
                            </div>
                            <div class="d-flex no-block align-items-center">
                                <div class="m-l-15">
                                    <h4 class="m-b-0"><?= $row['prod_name'] ?></h4>
                                    <span class="text-muted">globe type chair for rest</span>
                                </div>
                                <div class="ml-auto m-r-15">
                                    <button type="button" class="btn btn-dark btn-circle">$15</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
        }
    } elseif ($_POST['mat_ids'] != '' && $_POST['prd_ids'] == '') {
        $mat_ids = array();
        parse_str($_POST['mat_ids'], $mat_ids);
    
        foreach ($mat_ids as $m_ids) {
            $m_ids;
        }
    
        $material_id = implode("','", $m_ids);
    
        echo "<br>";
        if($short == 'ztoa'){
            $sql = "SELECT * FROM products WHERE mat_id IN ('$material_id') order by prod_name DESC";
        }
        else{
            $sql = "SELECT * FROM products WHERE mat_id IN ('$material_id') order by prod_name ASC";
        }
        $result = $connection->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
            ?>
                <div class="col-lg-4 col-md-12">
                    <div class="card">
                        <div class="el-card-item">
                            <div class="el-card-avatar el-overlay-1">
                                <?php
                                if ($row['drawings'] != '') {
                                    $imageNames = explode(":", $row['drawings']);
                                    $abc = $imageNames[0];
                                ?>
                                    <img class="product-image" src="uploads/products/<?= $abc ?>" alt="">
                                    <div class="el-overlay">
                                        <ul class="list-style-none el-info">
                                            <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="uploads/products/<?= $abc ?>"><i class="icon-magnifier"></i></a></li>
                                            <li class="el-item"><a class="btn default btn-outline el-link" href="product-single.php?id=<?= $row['prod_id'] ?>"><i class="icon-link"></i></a></li>
                                        </ul>
                                    </div>
                                <?php
                                } else {
                                ?>
                                    <img class="product-image" src="uploads/event_files/20191022071419525700.jpg" alt="">\
                                    <div class="el-overlay">
                                        <ul class="list-style-none el-info">
                                            <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="assets/images/gallery/chair.jpg"><i class="icon-magnifier"></i></a></li>
                                            <li class="el-item"><a class="btn default btn-outline el-link" href="product-single.php?id=<?= $row['prod_id'] ?>"><i class="icon-link"></i></a></li>
                                        </ul>
                                    </div>
    
                                <?php
                                }
    
                                ?>
    
    
                            </div>
                            <div class="d-flex no-block align-items-center">
                                <div class="m-l-15">
                                    <h4 class="m-b-0"><?= $row['prod_name'] ?></h4>
                                    <span class="text-muted">globe type chair for rest</span>
                                </div>
                                <div class="ml-auto m-r-15">
                                    <button type="button" class="btn btn-dark btn-circle">$15</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
        }
    } elseif ($_POST['mat_ids'] == '' && $_POST['prd_ids'] != '') {
        $prd_ids = array();
        parse_str($_POST['prd_ids'], $prd_ids);
    
        foreach ($prd_ids as $p_ids) {
            $p_ids;
        }
    
        $prdtype_id = implode("','", $p_ids);
    
        echo "<br>";
        if($short == 'ztoa'){
            $sql = "SELECT * FROM products WHERE prdtype_id IN ('$prdtype_id') order by prod_name DESC";
            
        }
        else{
            $sql = "SELECT * FROM products WHERE prdtype_id IN ('$prdtype_id') order by prod_name ASC";
        }
    
        $result = $connection->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
            ?>
                <div class="col-lg-4 col-md-12">
                    <div class="card">
                        <div class="el-card-item">
                            <div class="el-card-avatar el-overlay-1">
                                <?php
                                if ($row['drawings'] != '') {
                                    $imageNames = explode(":", $row['drawings']);
                                    $abc = $imageNames[0];
                                ?>
                                    <img class="product-image" src="uploads/products/<?= $abc ?>" alt="">
                                    <div class="el-overlay">
                                        <ul class="list-style-none el-info">
                                            <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="uploads/products/<?= $abc ?>"><i class="icon-magnifier"></i></a></li>
                                            <li class="el-item"><a class="btn default btn-outline el-link" href="product-single.php?id=<?= $row['prod_id'] ?>"><i class="icon-link"></i></a></li>
                                        </ul>
                                    </div>
                                <?php
                                } else {
                                ?>
                                    <img class="product-image" src="uploads/event_files/20191022071419525700.jpg" alt="">\
                                    <div class="el-overlay">
                                        <ul class="list-style-none el-info">
                                            <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="assets/images/gallery/chair.jpg"><i class="icon-magnifier"></i></a></li>
                                            <li class="el-item"><a class="btn default btn-outline el-link" href="product-single.php?id=<?= $row['prod_id'] ?>"><i class="icon-link"></i></a></li>
                                        </ul>
                                    </div>
    
                                <?php
                                }
    
                                ?>
    
    
                            </div>
                            <div class="d-flex no-block align-items-center">
                                <div class="m-l-15">
                                    <h4 class="m-b-0"><?= $row['prod_name'] ?></h4>
                                    <span class="text-muted">globe type chair for rest</span>
                                </div>
                                <div class="ml-auto m-r-15">
                                    <button type="button" class="btn btn-dark btn-circle">$15</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
        }
    } else {
    if($short == 'ztoa'){
            $sql = "SELECT * FROM products order by prod_name DESC";
        }
        else{
            $sql = "SELECT * FROM products order by prod_name ASC";
        }
        $result = $connection->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
            ?>
                <div class="col-lg-4 col-md-12">
                    <div class="card">
                        <div class="el-card-item">
                            <div class="el-card-avatar el-overlay-1">
                                <?php
                                if ($row['drawings'] != '') {
                                    $imageNames = explode(":", $row['drawings']);
                                    $abc = $imageNames[0];
                                ?>
                                    <img class="product-image" src="uploads/products/<?= $abc ?>" alt="">
                                    <div class="el-overlay">
                                        <ul class="list-style-none el-info">
                                            <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="uploads/products/<?= $abc ?>"><i class="icon-magnifier"></i></a></li>
                                            <li class="el-item"><a class="btn default btn-outline el-link" href="product-single.php?id=<?= $row['prod_id'] ?>"><i class="icon-link"></i></a></li>
                                        </ul>
                                    </div>
                                <?php
                                } else {
                                ?>
                                    <img class="product-image" src="uploads/event_files/20191022071419525700.jpg" alt="">\
                                    <div class="el-overlay">
                                        <ul class="list-style-none el-info">
                                            <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="assets/images/gallery/chair.jpg"><i class="icon-magnifier"></i></a></li>
                                            <li class="el-item"><a class="btn default btn-outline el-link" href="product-single.php?id=<?= $row['prod_id'] ?>"><i class="icon-link"></i></a></li>
                                        </ul>
                                    </div>
    
                                <?php
                                }
                                ?>
                            </div>
                            <div class="d-flex no-block align-items-center">
                                <div class="m-l-15">
                                    <h4 class="m-b-0"><?= $row['prod_name'] ?></h4>
                                    <span class="text-muted">globe type chair for rest</span>
                                </div>
                                <div class="ml-auto m-r-15">
                                    <button type="button" class="btn btn-dark btn-circle">$15</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    <?php
            }
        }
    }
}

?>
<?php 
include("dbconfig.php");
 
if ($connection->connect_error) { 
    die("Connection failed: " . $connection->connect_error); 
} 
 
// Get search term 
$searchTerm = $_GET['term']; 
 
// Fetch matched data from the database 
$query = $connection->query("SELECT * FROM employees WHERE emp_name LIKE '%".$searchTerm."%'  ORDER BY emp_name ASC"); 
 
// Generate array with skills data 
$skillData = array(); 
if($query->num_rows > 0){ 
    while($row = $query->fetch_assoc()){ 
        $data['id'] = $row['id']; 
        $data['value'] = $row['emp_name']; 
        array_push($skillData, $data); 
    } 
} 

echo json_encode($skillData); 
?>
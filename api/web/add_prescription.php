<?php
session_start();
if(!isset($_SESSION['logged_in'])) {
   header("Location:../login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<!-- Mirrored from radixtouch.in/templates/admin/sunray/source/light/add_doctor.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 19 Nov 2018 10:05:30 GMT -->
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Responsive Admin Template" />
    <meta name="author" content="Sunray" />
    <title>Sunray | Bootstrap Responsive Hospital Admin Template</title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
  <!-- icons -->
    <link href="../fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--bootstrap -->
  <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
      <link href="../assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
  <!-- Material Design Lite CSS -->
  <link href="../assets/plugins/material/material.min.css" rel="stylesheet" >
  <link href="../assets/css/material_style.css" rel="stylesheet">
  <!-- Theme Styles -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />  
    <link href="../assets/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <link href="../assets/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
  <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
  <link href="../assets/css/theme-color.css" rel="stylesheet" type="text/css" />
    <!-- sweet alert-->
  <!--   <link rel="stylesheet" type="text/css" href="../sweet/css/app.css">
    <link rel="stylesheet" type="text/css" href="../sweet/css/guide.css">
    <link rel="stylesheet" type="text/css" href="../sweet/css/header.css">
    <link rel="stylesheet" type="text/css" href="../sweet/css/highlight.css">
    <link rel="stylesheet" type="text/css" href="../sweet/css/index.css">
    <link rel="stylesheet" type="text/css" href="../sweet/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="../sweet/css/table.css">
    <link rel="stylesheet" type="text/css" href="../sweet/css/variables.css"> -->
     <link rel="stylesheet" type="text/css" href="../sweet/css/sweetalert.css">
    

  <!-- favicon -->
  <link rel="shortcut icon" href="http://radixtouch.in/templates/admin/sunray/source/assets/img/favicon.ico" /> 

</head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-color logo-dark">
    <div class="page-wrapper">
        <!-- start header -->
    <?php include 'header.php';?>
        <!-- end header -->
        <!-- start page container -->
        <div class="page-container">
      <!-- start sidebar menu -->
      <?php include 'side.php';?>
       <!-- end sidebar menu -->
      <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class="row">
                                             <div class="col-md-3"><a href="all_doctor.php" class="btn btn-sm btn-link m-b-5"><i class="fa fa-arrow-left m-t-5"></i> Bact to doctor </a></div>
                           <!--  <div class=" pull-left">
                                <div class="page-title">View Doctor</div>
                            </div> -->
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li><a class="parent-item" href="#">Doctors</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">View Doctor</li>
                            </ol>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="card card-box">
                                <div class="card-head">
                                    <header>Doctor Information</header>
                                     <button id = "panel-button" 
                                   class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
                                   data-upgraded = ",MaterialButton">
                                   <i class = "material-icons">more_vert</i>
                                </button>
                                <ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                   data-mdl-for = "panel-button">
                                   <li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
                                   <li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
                                   <li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
                                </ul>
                                </div>
                                <div class="card-body" id="bar-parent">
                                   <!--  <form  action="../api/web/ak.php" class="form-horizontal" method="POST" id="add_doctor_form">
                                        <div class="form-body">
                                        <div class="form-group row">
                                                <label class="control-label col-md-3">First Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" required name="firstname" data-required="1" placeholder="enter first name" class="form-control input-height" /> </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">Last Name
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" required name="lastname" data-required="1" placeholder="enter last name" class="form-control input-height" /> </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">Email
                                                </label>
                                                <div class="col-md-5">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                                <i class="fa fa-envelope"></i>
                                                            </span>
                                                        <input type="text" required class="form-control input-height" name="email" placeholder="Email Address"> </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">Password
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="password" required name="pswd" data-required="1" placeholder="enter Password" class="form-control input-height" /> </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">Confirm Password
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="password" required name="cnfmPwd" data-required="1" placeholder="Reenter your password" class="form-control input-height" /> </div>
                                            </div>
                                           <div class="form-group row">
                                                <label class="control-label col-md-3">Designation
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="designation" data-required="1" placeholder="enter your designation" class="form-control input-height" /> </div>
                                            </div> -->
                                           <!--  <div class="form-group row">
                                                <label class="control-label col-md-3">Departments
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <select class="form-control input-height" name="department">
                                                        <option value="">Select...</option>
                                                        <option value="Category 1">Neurology</option>
                                                        <option value="Category 2">Orthopedics</option>
                                                        <option value="Category 3">Gynaecology</option>
                                                        <option value="Category 3">Microbiology</option>
                                                        <option value="Category 3">Radiotherapy</option>
                                                        <option value="Category 3">Pharmacy</option>
                                                    </select>
                                                </div>
                                            </div> -->
                                           <!--  <div class="form-group row">
                                                <label class="control-label col-md-3">Gender
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <select class="form-control input-height" required name="gender">
                                                        <option value="">Select...</option>
                                                        <option value="Male">Male</option>
                                                        <option value="Female">Female</option>
                                                    </select>
                                                </div>
                                            </div> -->
                                           <!--  <div class="form-group row">
                                                <label class="control-label col-md-3">Mobile No.
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input name="number" required type="text" placeholder="mobile number" class="form-control input-height" /> </div>
                                            </div>
                                               <div class="form-group row">
                                                <label class="control-label col-md-3">Date Of Birth
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <div class="input-group date form_date " data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                                <input class="form-control input-height" required size="16" placeholder="date of Birth" type="text" value="">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            </div>
                                            <input type="hidden" id="dtp_input2" value="" />
                                                </div>
                                            </div> -->
                                           <!-- <div class="form-group row">
                                                <label class="control-label col-md-3">Address
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <textarea name="address" placeholder="address" class="form-control-textarea" rows="5" ></textarea>
                                                </div>
                                            </div> -->
                                           <!--  <div class="form-group row">
                                                <label class="control-label col-md-3">Profile Picture
                                                </label>
                                                <div class="compose-editor">
                                                  <input type="file" class="default" multiple>
                                              </div>
                                            </div> -->
                                 <form class="form-horizontal" >
                                <div class="form-body">
                                    <div class="card-body">
                                        <h4 class="card-title"></h4>
                                    </div>
                                    <!-- <hr class="m-t-0 m-b-40"> -->
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row" >
                                                    <label class="control-label text-right text-dark col-md-3 m-t-5">Email:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c1">  </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row" >
                                                    <label class="control-label text-right text-dark col-md-3 m-t-5">Password:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c2">  </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right text-dark col-md-3 m-t-5">Role Id:</label>
                                                    <div class="col-md-9" >
                                                        <p class="form-control text-dark" id="c3">  </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right text-dark col-md-3 m-t-5" >Name:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c4"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right text-dark col-md-3 m-t-5"> Gender:</label>
                                                    <div class="col-md-9" >
                                                        <p class="form-control text-dark" id="c5">  </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row" >
                                                    <label class="control-label text-right text-dark col-md-3 m-t-5">Mobile:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c6">  </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row" >
                                                    <label class="control-label text-right text-dark col-md-3 m-t-5">Date Of Birth:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c7">  </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <!-- <div class="col-md-6">
                                                <div class="form-group row" >
                                                    <label class="control-label text-right text-dark col-md-3 m-t-5">Contact:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c8">  </p>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!--/span-->
                                        </div>


                                         <!--   <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row" >
                                                    <label class="control-label text-right text-dark col-md-3 m-t-5">Department:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c9">  </p>
                                                    </div>
                                                </div>
                                            </div>
                                            /span
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right text-dark col-md-3 m-t-5">Email:</label>
                                                    <div class="col-md-9" >
                                                        <p class="form-control text-dark" id="c10">  </p>
                                                    </div>
                                                </div>
                                            </div>
                                            /span
                                        </div> -->
 
                                               
                                       <!--  <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right text-dark col-md-3 m-t-5">Mobile1:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c11">  </p>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right text-dark col-md-3 m-t-5">Mobile2:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c12">  </p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div> -->

                                       <!--     <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right text-dark col-md-3 m-t-5">Whatsapp:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c13">  </p>
                                                    </div>
                                                </div>
                                            </div>
                                          <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right text-dark col-md-3 m-t-5">Remark:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c14">  </p>
                                                    </div>
                                                </div>
                                            </div>
                                       
                                        </div>


                                         <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right text-dark col-md-3 m-t-5">Enquiry No:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c15">  </p>
                                                    </div>
                                                </div>
                                            </div>
                                                                                        <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right text-dark col-md-3 m-t-5">Product:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static text-dark" id="c16">  </p>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                        </div> -->

 
                                        <!--  <div class="row"> -->
                                            <!-- <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right col-md-3">Product Details:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"> Category1 </p>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!--/span-->
                                            <!-- <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right text-dark col-md-3 m-t-5">Added At:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark " id="c17">  </p>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!--/span-->
                                      <!--   </div> -->






                                        <!--/row-->
                                       <!--  <h4 class="card-title">Address</h4> -->
                                    </div>
                                   <!--  <hr class="m-t-0 m-b-40"> -->
                                   
                                   <!--  <hr> -->
                                    <div class="form-actions">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <!-- <div class="col-md-offset-3 col-md-9">
                                                            <button type="submit" class="btn btn-danger"> <i class="fa fa-pencil"></i> Edit</button>
                                                            <button type="button" class="btn btn-dark">Cancel</button>
                                                        </div> -->
                                                    </div>
                                                </div>
                                                <div class="col-md-6"> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>





                      <!-- <div class="form-group row">
                        <label class="control-label col-md-3">Education 
                        </label>
                        <div class="col-md-5">
                          <textarea name="address" class="form-control-textarea" placeholder="Education" rows="5"></textarea>
                        </div>
                      </div> -->
                      <div class="form-actions">
                                            <div class="row">
                                                <!-- <div class="offset-md-3 col-md-9">
                                                    <button type="submit" class="btn btn-info"  >Submit</button>
                                                    <button type="button" class="btn btn-default">Cancel</button>
                                                </div> -->
                                              </div>
                                           </div>
                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page content -->
            <!-- start chat sidebar -->
            <div class="chat-sidebar-container" data-close-on-body-click="false">
                <div class="chat-sidebar">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon"  data-toggle="tab">Theme</a>
                        </li>
                        <li class="nav-item">
                            <a href="#quick_sidebar_tab_2" class="nav-link tab-icon"  data-toggle="tab">Settings</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <!-- Start Color Theme Sidebar -->
                      <div class="tab-pane chat-sidebar-settings in show active animated shake" role="tabpanel" id="quick_sidebar_tab_1">
              <div class="chat-sidebar-slimscroll-style">
                <div class="theme-light-dark">
                  <h6>Sidebar Theme</h6>
                  <button type="button" data-theme="white" class="btn lightColor btn-outline btn-circle m-b-10 theme-button">Light Sidebar</button>
                  <button type="button" data-theme="dark" class="btn dark btn-outline btn-circle m-b-10 theme-button">Dark Sidebar</button>
                </div>
                <div class="theme-light-dark">
                  <h6>Sidebar Color</h6>
                  <ul class="list-unstyled">
                    <li class="complete">
                      <div class="theme-color sidebar-theme">
                        <a href="#" data-theme="white"><span class="head"></span><span class="cont"></span></a>
                        <a href="#" data-theme="dark"><span class="head"></span><span class="cont"></span></a>
                        <a href="#" data-theme="blue"><span class="head"></span><span class="cont"></span></a>
                        <a href="#" data-theme="indigo"><span class="head"></span><span class="cont"></span></a>
                        <a href="#" data-theme="cyan"><span class="head"></span><span class="cont"></span></a>
                        <a href="#" data-theme="green"><span class="head"></span><span class="cont"></span></a>
                        <a href="#" data-theme="red"><span class="head"></span><span class="cont"></span></a>
                      </div>
                    </li>
                  </ul>
                  <h6>Header Brand color</h6>
                  <ul class="list-unstyled">
                    <li class="theme-option">
                      <div class="theme-color logo-theme">
                              <a href="#" data-theme="logo-white"><span class="head"></span><span class="cont"></span></a>
                        <a href="#" data-theme="logo-dark"><span class="head"></span><span class="cont"></span></a>
                        <a href="#" data-theme="logo-blue"><span class="head"></span><span class="cont"></span></a>
                        <a href="#" data-theme="logo-indigo"><span class="head"></span><span class="cont"></span></a>
                        <a href="#" data-theme="logo-cyan"><span class="head"></span><span class="cont"></span></a>
                        <a href="#" data-theme="logo-green"><span class="head"></span><span class="cont"></span></a>
                        <a href="#" data-theme="logo-red"><span class="head"></span><span class="cont"></span></a>
                            </div>
                        </li>
                  </ul>
                  <h6>Header color</h6>
                  <ul class="list-unstyled">
                    <li class="theme-option">
                      <div class="theme-color header-theme">
                              <a href="#" data-theme="header-white"><span class="head"></span><span class="cont"></span></a>
                              <a href="#" data-theme="header-dark"><span class="head"></span><span class="cont"></span></a>
                              <a href="#" data-theme="header-blue"><span class="head"></span><span class="cont"></span></a>
                              <a href="#" data-theme="header-indigo"><span class="head"></span><span class="cont"></span></a>
                              <a href="#" data-theme="header-cyan"><span class="head"></span><span class="cont"></span></a>
                              <a href="#" data-theme="header-green"><span class="head"></span><span class="cont"></span></a>
                              <a href="#" data-theme="header-red"><span class="head"></span><span class="cont"></span></a>
                            </div>
                        </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End Color Theme Sidebar -->
            <!-- Start Setting Panel --> 
            <div class="tab-pane chat-sidebar-settings" role="tabpanel" id="quick_sidebar_tab_2">
                           <div class="chat-sidebar-settings-list chat-sidebar-slimscroll-style">
                                <div class="chat-header"><h5 class="list-heading">Layout Settings</h5></div>
                              <div class="chatpane inner-content ">
                  <div class="settings-list">
                              <div class="setting-item">
                                  <div class="setting-text">Sidebar Position</div>
                                  <div class="setting-set">
                                     <select class="sidebar-pos-option form-control input-inline input-sm input-small ">
                                                  <option value="left" selected="selected">Left</option>
                                                  <option value="right">Right</option>
                                              </select>
                                  </div>
                              </div>
                              <div class="setting-item">
                                  <div class="setting-text">Header</div>
                                  <div class="setting-set">
                                     <select class="page-header-option form-control input-inline input-sm input-small ">
                                                  <option value="fixed" selected="selected">Fixed</option>
                                                  <option value="default">Default</option>
                                              </select>
                                  </div>
                              </div>
                              <div class="setting-item">
                                  <div class="setting-text">Sidebar Menu </div>
                                  <div class="setting-set">
                                     <select class="sidebar-menu-option form-control input-inline input-sm input-small ">
                                                  <option value="accordion" selected="selected">Accordion</option>
                                                  <option value="hover">Hover</option>
                                              </select>
                                  </div>
                              </div>
                              <div class="setting-item">
                                  <div class="setting-text">Footer</div>
                                  <div class="setting-set">
                                     <select class="page-footer-option form-control input-inline input-sm input-small ">
                                                  <option value="fixed">Fixed</option>
                                                  <option value="default" selected="selected">Default</option>
                                              </select>
                                  </div>
                              </div>
                          </div>
                  <div class="chat-header"><h5 class="list-heading">Account Settings</h5></div>
                  <div class="settings-list">
                              <div class="setting-item">
                                  <div class="setting-text">Notifications</div>
                                  <div class="setting-set">
                                      <div class="switch">
                                          <label class = "mdl-switch mdl-js-switch mdl-js-ripple-effect" 
                                    for = "switch-1">
                                    <input type = "checkbox" id = "switch-1" 
                                       class = "mdl-switch__input" checked>
                                  </label>
                                      </div>
                                  </div>
                              </div>
                              <div class="setting-item">
                                  <div class="setting-text">Show Online</div>
                                  <div class="setting-set">
                                      <div class="switch">
                                          <label class = "mdl-switch mdl-js-switch mdl-js-ripple-effect" 
                                    for = "switch-7">
                                    <input type = "checkbox" id = "switch-7" 
                                       class = "mdl-switch__input" checked>
                                  </label>
                                      </div>
                                  </div>
                              </div>
                              <div class="setting-item">
                                  <div class="setting-text">Status</div>
                                  <div class="setting-set">
                                      <div class="switch">
                                          <label class = "mdl-switch mdl-js-switch mdl-js-ripple-effect" 
                                    for = "switch-2">
                                    <input type = "checkbox" id = "switch-2" 
                                       class = "mdl-switch__input" checked>
                                  </label>
                                      </div>
                                  </div>
                              </div>
                              <div class="setting-item">
                                  <div class="setting-text">2 Steps Verification</div>
                                  <div class="setting-set">
                                      <div class="switch">
                                          <label class = "mdl-switch mdl-js-switch mdl-js-ripple-effect" 
                                    for = "switch-3">
                                    <input type = "checkbox" id = "switch-3" 
                                       class = "mdl-switch__input" checked>
                                  </label>
                                      </div>
                                  </div>
                              </div>
                          </div>
                                    <div class="chat-header"><h5 class="list-heading">General Settings</h5></div>
                                    <div class="settings-list">
                              <div class="setting-item">
                                  <div class="setting-text">Location</div>
                                  <div class="setting-set">
                                      <div class="switch">
                                          <label class = "mdl-switch mdl-js-switch mdl-js-ripple-effect" 
                                    for = "switch-4">
                                    <input type = "checkbox" id = "switch-4" 
                                       class = "mdl-switch__input" checked>
                                  </label>
                                      </div>
                                  </div>
                              </div>
                              <div class="setting-item">
                                  <div class="setting-text">Save Histry</div>
                                  <div class="setting-set">
                                      <div class="switch">
                                          <label class = "mdl-switch mdl-js-switch mdl-js-ripple-effect" 
                                    for = "switch-5">
                                    <input type = "checkbox" id = "switch-5" 
                                       class = "mdl-switch__input" checked>
                                  </label>
                                      </div>
                                  </div>
                              </div>
                              <div class="setting-item">
                                  <div class="setting-text">Auto Updates</div>
                                  <div class="setting-set">
                                      <div class="switch">
                                          <label class = "mdl-switch mdl-js-switch mdl-js-ripple-effect" 
                                    for = "switch-6">
                                    <input type = "checkbox" id = "switch-6" 
                                       class = "mdl-switch__input" checked>
                                  </label>
                                      </div>
                                  </div>
                              </div>
                          </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <!-- start footer -->
       <?php include 'footer.php';?>
        <!-- end footer -->
    </div>
    <!-- start js include path -->
    <script src="../assets/plugins/jquery/jquery.min.js" ></script>
  <script src="../assets/plugins/popper/popper.min.js" ></script>
    <script src="../assets/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
    <script src="../assets/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
    <script src="../assets/plugins/jquery-validation/js/additional-methods.min.js" ></script>
    <script src="../assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <!-- bootstrap -->
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <script src="../assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script src="../assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js"></script>
    <!-- Common js-->
  <script src="../assets/js/app.js" ></script>
    <script src="../assets/js/pages/validation/form-validation.js" ></script>
    <script src="../assets/js/layout.js" ></script>
  <script src="../assets/js/theme-color.js" ></script>
  <!-- Material -->
  <script src="../assets/plugins/material/material.min.js"></script>
   <!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.10.2/sweetalert2.all.min.js"></script> -->
   <script src="../sweet/js/sweetalert.min.js"></script>
  
    <<!-- script>
      
$("#add_doctor_form").submit(function(e) {

    e.preventDefault(); // avoid to execute the actual submit of the form.

    var form = $(this);
    var url = form.attr('action');

    $.ajax({
           type: "POST",
           url: url,
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
               // alert(data); // show response from the php script.
                                   if(data.trim()=="success"){
                       swal("Done", "Data inserted sucessfully.............", "success");
                         }
                          else{
                         swal("Fail!", data, "warning");
                         }
           }
         });


});
</script> -->
<script src="../assets/plugins/datatables/jquery.dataTables.min.js" ></script>
  <script src="../assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
<!-- <script>
    $(document).ready(function (){
   $('#doctor-datatable').DataTable({
      lengthChange: false,
      ajax: {
          url: "../api/web/view_doctor.php",
          dataSrc: "",
      },
      columns: [
           
         
          { data: "name" },
           { data: "user_email"}, 
          { data: "gender"}, 
          { data: "mobile"}, 
          // { data: "cpassword"}, 
          // { data: "gender"}, 
          // { data: "mobile"}, 
          // { data: "dob"}, 
           {
        "render": function ( data, type, row) {
      return '<a class="btn btn-sm btn-success" href="view_enquiry.php?id=' + row.user_id + '"><i class="fa fa-search m-r-5"></i>Details</a>';                 
            }
        }

        ],
      select: true
   });
});
 
  </script> -->
     <!-- end js include path -->
     <script>
   //  $(document).ready(function (){
   // $('#doctor-datatable').DataTable({
   //    lengthChange: false,
   //    ajax: {
   //        url: "../api/web/view_doctor.php",
   //        dataSrc: "",
   //    },
   //    columns: [
           
   //        { data: "user_email" },
   //        { data: "password"}, 
   //        { data: "role_id"}, 
   //        { data: "name"}, 
   //        { data: "gender" },
   //        { data: "mobile" },
   //        { data: "dob" },


          
         
      //      {
      //   "render": function ( data, type, row) {
      // return '<a class="btn btn-sm btn-success" href="view_enquiry.php?id=' + row.user_id + '"><i class="fa fa-search m-r-5"></i>Details</a>';                 
      //       }
        

//         ],
//       select: true
//    });
// });




 $.ajax({
           type: "GET",
           url: "../api/web/view_doctor.php",
           data: {id:"<?=$_GET['id']?>"}, // serializes the form's elements.
           success: function(data)
           {
               // alert(data); // show response from the php script.
            //  if(data.trim()=="success"){
            //            swal("Done", "Data inserted sucessfully.............", "success");
            // }
            //               else{
            //              swal("Fail!", data, "warning");
            //              }
           // alert (data[0]["ce_id"]);
           document.getElementById("c1").innerHTML=data[0]["user_email"];
           document.getElementById("c2").innerHTML=data[0]["password"];
           document.getElementById("c3").innerHTML=data[0]["role_id"];
           document.getElementById("c4").innerHTML=data[0]["name"];
           document.getElementById("c5").innerHTML=data[0]["gender"];
           document.getElementById("c6").innerHTML=data[0]["mobile"];
           document.getElementById("c7").innerHTML=data[0]["dob"];
           }
         });
//    $(document).ready(function (){
//    $('#customer-datatable').DataTable({
//       lengthChange: false,
//       ajax: {
//           url: "api/web/show-all-customers.php",
//           dataSrc: "",
//       },
//       columns: [
           
//           { data: "cust_name"}, 
//           { data: "division" },
//           { data: "department"}, 
//           { data: "enquiry_no"}, 
//           { data: "product_details"}, 
//           { data: "added_at"}, 
         
//            {
//        "render": function ( data, type, row) {
//      return '<a class="btn btn-circle btn-primary" href="view_enqueiry.php?id=' + row.cust_id + '" style="width:75px;"><i class="fa fa-search"></i>View</a>';                 
//             }
//         }

//         ],
//       select: true
//    });
// });



  </script>
 
  </script>
</body>

<!-- Mirrored from radixtouch.in/templates/admin/sunray/source/light/add_doctor.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 19 Nov 2018 10:05:30 GMT -->
</html>
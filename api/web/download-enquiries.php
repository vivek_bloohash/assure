<?php
session_start();
require("../../libs/phpexcel/PHPExcel.php");
$phpExcel = new PHPExcel;
 
// Setting font to Arial Black
 
$phpExcel->getDefaultStyle()->getFont()->setName('Arial');
 
// Setting font size to 14
 
$phpExcel->getDefaultStyle()->getFont()->setSize(8);
 
//Setting description, creator and title
 
$phpExcel ->getProperties()->setTitle("Enquiries List");
 
$phpExcel ->getProperties()->setCreator("Assure Solutions");
 
$phpExcel ->getProperties()->setDescription("Customer Enquiries List");
 
// Creating PHPExcel spreadsheet writer object
 
// We will create xlsx file (Excel 2007 and above)
 
$writer = PHPExcel_IOFactory::createWriter($phpExcel, "Excel2007");
 
// When creating the writer object, the first sheet is also created
 
// We will get the already created sheet
 
$sheet = $phpExcel ->getActiveSheet();
 
// Setting title of the sheet
 
$sheet->setTitle('Customer list');
 
// Creating spreadsheet header
 
$sheet ->getCell('A1')->setValue('Srno');
 
$sheet ->getCell('B1')->setValue('Customer Name');
 
$sheet ->getCell('C1')->setValue('Division');

$sheet ->getCell('D1')->setValue('Location');

$sheet ->getCell('E1')->setValue('Work Address');

//$sheet ->getCell('F1')->setValue('Address'); 
$sheet ->getCell('F1')->setValue('Work Pin');
$sheet ->getCell('G1')->setValue('Office Address');
$sheet ->getCell('H1')->setValue('Office Pin');

$sheet ->getCell('I1')->setValue('Contact Person');

$sheet ->getCell('J1')->setValue('Department');
$sheet ->getCell('K1')->setValue('Email');
$sheet ->getCell('L1')->setValue('Mobile 1');
$sheet ->getCell('M1')->setValue('Mobile 2');
$sheet ->getCell('N1')->setValue('Whatsapp');
$sheet ->getCell('O1')->setValue('Remarks');
$sheet ->getCell('P1')->setValue('Enquiry No');
$sheet ->getCell('Q1')->setValue('Product Details');

// Making headers text bold and larger
 
$sheet->getStyle('A1:R1')->getFont()->setBold(true)->setSize(8);
 
include("dbconfig.php");
// Insert product data
//$today = date("Y-m-d");
//$today = "2018-06-27";
$query = "select * from customer_enquiries";
$result = mysqli_query($connection,$query);
if(mysqli_num_rows($result) < 1) {
	//$_SESSION['no_record'] = 1;
	header("Location:display-enquiries.php");
	exit;
}
$rowNum = 2;
$srno = 1;
while($row = mysqli_fetch_array($result))  {
	//print_r($row);
	//echo "<br><br>";
	$sheet ->getCell('A'.$rowNum)->setValue($srno);
	$sheet ->getCell('B'.$rowNum)->setValue($row['cust_name']);
	$sheet ->getCell('C'.$rowNum)->setValue($row['division']);
	$sheet ->getCell('D'.$rowNum)->setValue($row['location']);
	$sheet ->getCell('E'.$rowNum)->setValue($row['work_address']);
	$sheet ->getCell('F'.$rowNum)->setValue($row['work_pin']);
	$sheet ->getCell('G'.$rowNum)->setValue($row['office_address']);
	$sheet ->getCell('H'.$rowNum)->setValue($row['office_pin']);
	$sheet ->getCell('I'.$rowNum)->setValue($row['contact_person']);
	$sheet ->getCell('J'.$rowNum)->setValue($row['department']);
	$sheet ->getCell('K'.$rowNum)->setValue($row['email']);
	$sheet ->getCell('L'.$rowNum)->setValue($row['mobile1']);
	$sheet ->getCell('M'.$rowNum)->setValue($row['mobile2']);
	$sheet ->getCell('N'.$rowNum)->setValue($row['whatsapp']);
	$sheet ->getCell('O'.$rowNum)->setValue($row['remarks']);
	$sheet ->getCell('P'.$rowNum)->setValue($row['enquiry_no']);
	$sheet ->getCell('Q'.$rowNum)->setValue($row['product_details']);
	$rowNum++;
	$srno++;	

}

// Size the columns
 
$sheet->getColumnDimension('A')->setAutoSize(true);
 
$sheet->getColumnDimension('B')->setAutoSize(true);
 
$sheet->getColumnDimension('C')->setAutoSize(true);
$sheet->getColumnDimension('D')->setAutoSize(true);
$sheet->getColumnDimension('E')->setWidth(10);
$sheet->getColumnDimension('F')->setAutoSize(true);
$sheet->getColumnDimension('G')->setWidth(10);
$sheet->getColumnDimension('H')->setAutoSize(true);
$sheet->getColumnDimension('I')->setAutoSize(true);
$sheet->getColumnDimension('J')->setAutoSize(true);
$sheet->getColumnDimension('K')->setAutoSize(true);
$sheet->getColumnDimension('L')->setAutoSize(true);
$sheet->getColumnDimension('N')->setAutoSize(true);
$sheet->getColumnDimension('M')->setAutoSize(true);
$sheet->getColumnDimension('N')->setAutoSize(true);
$sheet->getColumnDimension('O')->setWidth(10);
$sheet->getColumnDimension('P')->setAutoSize(true);
$sheet->getColumnDimension('Q')->setWidth(10);

//Wrap Text for some columns
$sheet->getStyle('E1:E'.$sheet->getHighestRow())
    ->getAlignment()->setWrapText(true); 
    
$sheet->getStyle('G1:G'.$sheet->getHighestRow())
    ->getAlignment()->setWrapText(true); 
$sheet->getStyle('O1:O'.$sheet->getHighestRow())
    ->getAlignment()->setWrapText(true); 
 
$sheet->getStyle('Q1:Q'.$sheet->getHighestRow())
    ->getAlignment()->setWrapText(true); 
// Save the spreadsheet
 
$writer->save('customer_enquiries'.$today.'.xlsx');
header("Location:"."customer_enquiries".$today.".xlsx");

?>
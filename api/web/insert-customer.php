<?php
include("dbconfig.php");
if(isset($_POST['division'])) {
  
    $division = mysqli_real_escape_string($connection,$_POST['division']);
    $cust_id = mysqli_real_escape_string($connection,$_POST['cust_id']);
    $contact_person = mysqli_real_escape_string($connection,$_POST['contact_person']);
    $department = mysqli_real_escape_string($connection,$_POST['department']);
    $designation = mysqli_real_escape_string($connection,$_POST['designation']);
    $email = mysqli_real_escape_string($connection,$_POST['email']);
    $mobile1 = mysqli_real_escape_string($connection,$_POST['mobile1']);
    $mobile2 = mysqli_real_escape_string($connection,$_POST['mobile2']);
    $whatsapp_no = mysqli_real_escape_string($connection,$_POST['whatsapp']);
    $remarks = mysqli_real_escape_string($connection,$_POST['remarks']);
    $enquiry_no = mysqli_real_escape_string($connection,$_POST['enquiry_no']);
    $product_detail = mysqli_real_escape_string($connection,$_POST['product_detail']);

    if( empty($division)  ||empty($cust_id) || empty($contact_person) || empty($department) || empty($email) || empty($mobile1) || empty($enquiry_no) || empty($product_detail)) {
        echo "Required fields cannot be blank.";
    }
    else if(strlen($mobile1) > 13 || strlen($mobile2) > 13 || strlen($whatsapp_no) > 13) {
        echo "Phone number should be less than 13 characters";
    }
    else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo "Please enter a valid email address";
    }
    else {
        $queryString = "insert into customer_enquiries(division,cust_id,contact_person,department,email,mobile1,mobile2,whatsapp,remarks,enquiry_no,product_details) values('$division','cust_id','$contact_person','$department','$email','$mobile1','$mobile2','$whatsapp_no','$remarks','$enquiry_no','$product_detail')";
        if(mysqli_query($connection,$queryString)) {
            echo "Success";
        }
        else {
            echo mysqli_error($connection);
            echo "An error occurred";
        }
    }


}

?>
<?php
include 'dbconfig.php';
if(!empty($_POST["id"])){

    $query = $connection->query("SELECT COUNT(*) as num_rows FROM `enquiry_review` WHERE id < ".$_POST['id']." and enq_id = ".$_POST['enq_id']." ORDER BY id DESC");
    $row = $query->fetch_assoc();
    $totalRowCount = $row['num_rows'];
    
    $showLimit = 2;
    
    $qry = "SELECT * FROM `enquiry_review` INNER JOIN users ON enquiry_review.username = users.email where enquiry_review.id < ".$_POST['id']." and enquiry_review.enq_id = ".$_POST['enq_id']." ORDER BY enquiry_review.id DESC LIMIT $showLimit";

    $query = $connection->query($qry);

    if($query->num_rows > 0){ 
        while($row = $query->fetch_assoc()){
            $postID = $row['id'];
    ?>
    <li class="timeline-inverted timeline-item">
        <div class="timeline-badge success"><img src="assets/images/users/1.jpg" alt="img" class="img-fluid"> </div>
        <div class="timeline-panel">
            <div class="timeline-heading">
                <h4 class="timeline-title"><?= $row['name'] ?></h4>
                <p><small class="text-muted"><i class="fa fa-clock-o"></i> <?php echo date('F j, Y, g:i a', strtotime($row['added_at']));?></small> </p>
            </div>
            <div class="timeline-body">
                <p><?php echo $row['review']; ?></p>
            </div>
        </div>
    </li>
    <?php } ?>
    <?php if($totalRowCount > $showLimit){ ?>

    <div class="show_more_main" id="show_more_main<?php echo $postID; ?>">
        <span id="<?php echo $postID; ?>" class="show_more btn btn-default" title="Load more posts btn">Show more</span>
        <span class="loding" style="display: none;"><span class="loding_txt">Loading...</span></span>
    </div>
    <?php } ?>
    <?php
        }
    }
?>
<?php
session_start();
if(!isset($_SESSION['logged_in'])) {
    header("Location:index.php");
}
include 'api/web/dbconfig.php';
?>

<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>All Vendors</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/xtremeadmin/" />
    <!-- Custom CSS -->
    <link href="dist/css/style.min.css" rel="stylesheet">
    <link href="sweetalert/sweetalert.css" rel="stylesheet">
    <link href="assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
        href="assets/extra-libs/datatables.net-bs4/css/responsive.dataTables.min.css">
    <!-- <link href="sweetalert/sweetalert.css" rel="stylesheet"> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include "header.php"; ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php include "sidebar.php"; ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <!-- <h4 class="page-title">Customer Enquiry</h4> -->

                        <!-- <div class="d-flex align-items-center">
							<nav aria-label="breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="#">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Customer Enquiry</li>
								</ol>
							</nav>
						</div> -->
                    </div>
                    <div class="col-7 align-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <div class="m-r-10">

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- <div class="col-md-"></div> -->
                    <div class="col-md-12">
                        <div class="card">
                            <!-- <div class="card-body align-center">
								Customer Enquiry
							  <div class="row"> -->
                            <div class="col-sm-12">
                                <div class="card card-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h4 class="card-title">All Vendors </h4>
                                        </div>
                                        <div class="col-sm-6"><a class="btn btn-default"
                                                style="float: right; margin-bottom:20px" href="add-new-vendor.php">Add
                                                New Vendor</a></div>
                                    </div>


                                    <div class="table-responsive">
                                        <table id="customer-datatable" class="table table-striped table-bordered"
                                            cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Action</th>
                                                    <th>Vendor Name</th>
                                                    <th>Location</th>
                                                    <th>Product</th>
                                                    <th>Person</th>
                                                    <th>Email</th>
                                                    <th>Mobile1</th>
                                                    <th>Mobile2</th>
                                                    <th>Mobile3</th>
                                                    <th>Mobile4</th>
                                                    <th>Whatsapp</th>
                                                    <!-- th><button onclick="" class="btn btn-sm btn-success "><i class="fa fa-check m-r-5"></i>View</button></th> -->
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8 col-offset-md-2">
                                        <button type="button" id="formButton" class="btn btn-success text-center"
                                            style="display: block; margin: 0 auto;">Add remark</button>
                                        <form id="add-review-form" url="api/web/save-task-review.php"
                                            style="margin-top: 50px">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label
                                                            class="control-label text-right text-dark col-md-3 m-t-5">Add
                                                            remark:</label>
                                                        <div class="col-md-9">
                                                            <textarea rows="4" cols="50"
                                                                class="form-control text-dark col-md-9 m-t-5"
                                                                name="review" id="review"></textarea>
                                                            <input class="form-control text-dark col-md-9 m-t-5"
                                                                type="hidden" name="username" id="username"
                                                                value="<?= $_SESSION['username']?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-9">
                                                            <input class="form-control text-dark col-md-9 m-t-5"
                                                                value="<?=$_GET['id']?>" type="hidden" name="enq_id"
                                                                id="enq_id">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <input type="submit" value="Save" class="btn btn-success submit_pg">
                                        </form>
                                    </div>
                                </div>
                                <div class="d-md-flex align-items-center">
                                    <div>
                                        <h4 class="card-title">Remarks</h4>
                                        <h5 class="card-subtitle">Related to this Subasks</h5>
                                    </div>
                                </div>
                                <ul class="timeline timeline-left postList">
                                    <?php
                                        $qry = "SELECT id, review, added_at, username, name FROM vendors_review LEFT JOIN users ON vendors_review.username = users.email  ORDER BY vendors_review.id DESC LIMIT 2";
                                        // echo $qry;
                                        $query = $connection->query($qry);
                                        if($query->num_rows > 0){
                                        while($row = $query->fetch_assoc()){
                                        $postID = $row['id'];
                                    ?>
                                    <li class="timeline-inverted timeline-item">
                                        <div class="timeline-badge success"><img src="assets/images/users/1.jpg"
                                                alt="img" class="img-fluid">
                                        </div>
                                        <div class="timeline-panel">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title"><?= $row['name'] ?> </h4>
                                                <p><small class="text-muted"><i class="fa fa-clock-o"></i>

                                                        <?php echo date('F j, Y, g:i a', strtotime($row['added_at']));?>
                                                    </small> </p>
                                            </div>
                                            <div class="timeline-body">
                                                <p><?php echo $row['review']?></p>
                                            </div>
                                        </div>
                                    </li>
                                    <?php } ?>
                                    <div class="show_more_main" id="show_more_main<?php echo $postID; ?>">
                                        <span id="<?php echo $postID; ?>" class="show_more btn btn-default"
                                            title="Load more posts">Show more</span>
                                        <span class="loding" style="display: none;"><span
                                                class="loding_txt">Loading...</span></span>
                                    </div>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <?php include "footer.php"; ?>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- customizer Panel -->
    <!-- ============================================================== -->

    <div class="chat-windows"></div>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->


    <script src="assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="dist/js/app.min.js"></script>
    <script src="dist/js/app.init.light-sidebar.js"></script>
    <script src="dist/js/app-style-switcher.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>

    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <script src="sweetalert/sweetalert.min.js"></script>
    <script src="assets/extra-libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="assets/extra-libs/datatables.net-bs4/js/dataTables.responsive.min.js"></script>
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css"
        rel="stylesheet" />
    <script type="text/javascript"
        src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>
    <style>
    .btn.btn-danger.trash {
        color: #fff !important;
    }

    #add-review-form {
        display: none;
    }

    input.btn.btn-success.submit_pg {
        text-align: center;
        margin: 0 auto;
        display: block;
    }
    </style>
    <script>
    $(document).ready(function() {
        $("#formButton").click(function() {
            $("#add-review-form").toggle();
        });


        $(document).on('click', '.show_more', function() {
            var ID = $(this).attr('id');
            $('.show_more').hide();
            $('.loding').show();
            $.ajax({
                type: 'POST',
                url: 'api/web/load-vendors-review.php',
                data: {
                    id: ID,
                    enq_id: "<?=$_GET['id']?>"
                },
                success: function(html) {
                    $('#show_more_main' + ID).remove();
                    $('.postList').append(html);
                }
            });
        });


        $("#add-review-form").submit(function(e) {
            var form = $(this);
            var url = form.attr('action');
            var formCopy = document.getElementById('add-review-form');
            var formData = new FormData(formCopy);
            //var formData = 
            $.ajax({
                contentType: false,
                processData: false,
                type: "POST",
                url: "api/web/save-vendors-review.php",
                data: formData, // serializes the form's elements.

                success: function(data) {
                    //alert(data);
                    //document.getElementById("test-update-spinner").style.display = "none";
                    if (data == 'Success') {
                        swal({
                            title: "Done!",
                            text: "Remark was inserted successfully",
                            type: "success"
                        }, function() {
                            window.location = window.location;
                        });
                    } else {
                        swal("Failed!", data, "error");
                    }
                    /*document.getElementById('info_message').innerHTML = data;*/ // show response from the php script.
                }
            });

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });


        var dataRecords = $('#customer-datatable').DataTable({
            lengthChange: false,
            ajax: {
                url: "api/web/get-vendors.php",
                dataSrc: "",
            },
            columns: [{
                    "render": function(data, type, row) {
                        return '<a class="btn btn-primary trash" href="edit-vendor.php?id=' +
                            row.id +
                            '"><i class="fa fa-pencil-alt m-r-5"></i>Edit</a><a class="btn btn-danger trash" id=' +
                            row.id + '><i class="fa fa-trash m-r-5"></i>Delete</a>';
                    }
                },

                {
                    data: "name"
                },
                {
                    data: "location"
                },
                {
                    data: "product"
                },
                {
                    data: "person"
                },
                {
                    data: "email"
                },
                {
                    data: "mobile1"
                },
                {
                    data: "mobile2"
                },
                {
                    data: "mobile3"
                },
                {
                    data: "mobile4"
                },
                {
                    data: "whatsapp"
                },




            ],
            select: true
        });
        $("#customer-datatable").on('click', '.trash', function() {

            var del_id = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: 'api/web/delete_vendor.php',
                data: {
                    id: del_id
                },
                success: function(data) {
                    if (jQuery.trim(data) === "Success") {
                        swal({
                            title: "Success",
                            text: "Data Deleted.",
                            icon: "success"
                        });

                    }
                    dataRecords.ajax.reload();
                }
            })
        })


    });
    </script>
</body>

</html>
<?php
include("dbconfig.php");
if(isset($_POST['customer_name'])) {
    $customer_name = mysqli_real_escape_string($connection,$_POST['customer_name']);
    $division = mysqli_real_escape_string($connection,$_POST['division']);
    $location = mysqli_real_escape_string($connection,$_POST['location']);
    $work_address = mysqli_real_escape_string($connection,$_POST['work_address']);
    $work_pin = mysqli_real_escape_string($connection,$_POST['location']);
    $office_address = mysqli_real_escape_string($connection,$_POST['office_address']);
    $office_pin = mysqli_real_escape_string($connection,$_POST['office_pin']);
    $contact_person = mysqli_real_escape_string($connection,$_POST['contact_person']);
    $department = mysqli_real_escape_string($connection,$_POST['department']);
    $designation = mysqli_real_escape_string($connection,$_POST['designation']);
    $email = mysqli_real_escape_string($connection,$_POST['email']);
    $mobile1 = mysqli_real_escape_string($connection,$_POST['mobile1']);
    $mobile2 = mysqli_real_escape_string($connection,$_POST['mobile2']);
    $whatsapp_no = mysqli_real_escape_string($connection,$_POST['whatsapp']);
    $remarks = mysqli_real_escape_string($connection,$_POST['remarks']);
    $enquiry_no = mysqli_real_escape_string($connection,$_POST['enquiry_no']);
    $product_detail = mysqli_real_escape_string($connection,$_POST['product_detail']);

    if(empty($customer_name) || empty($division) || empty($location) || empty($work_address) || empty($work_pin) || empty($contact_person) || empty($department) || empty($email) || empty($mobile1) || empty($enquiry_no) || empty($product_detail)) {
        echo "Required fields cannot be blank.";
    }
    else if(strlen($mobile1) > 13 || strlen($mobile2) > 13 || strlen($whatsapp_no) > 13) {
        echo "Phone number should be less than 13 characters";
    }
    else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo "Please enter a valid email address";
    }
    else {
        $queryString = "insert into customer_enquiries(cust_name,division,location,work_address,work_pin,office_address,office_pin,contact_person,department,email,mobile1,mobile2,whatsapp,remarks,enquiry_no,product_details) values('$customer_name','$division','$location','$work_address','$work_pin','$office_address','$office_pin','$contact_person','$department','$email','$mobile1','$mobile2','$whatsapp_no','$remarks','$enquiry_no','$product_detail')";
        if(mysqli_query($connection,$queryString)) {
            echo "Success";
        }
        else {
            //echo mysqli_error($connection);
            echo "An error occurred";
        }
    }


}

?>
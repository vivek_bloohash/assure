<?php
session_start();

if (!isset($_SESSION['logged_in'])) {
    header("Location:index.php");
}
include("api/web/dbconfig.php");
if (!isset($_SESSION['logged_in'])) {
    header("Location:index.php");
}

date_default_timezone_set('Asia/Calcutta');


?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Product Page</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/xtremeadmin/" />
    <!-- Custom CSS -->
    <link href="dist/css/style.min.css" rel="stylesheet">
    <link href="sweetalert/sweetalert.css" rel="stylesheet">
    <link href="assets/libs/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">

    <!-- <link href="sweetalert/sweetalert.css" rel="stylesheet"> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    img.img-responsive {
        height: 320px;
        width: 223px;
    }
    </style>
    <style>
    ul#ui-id-1,
    ul#ui-id-2 {
        background-color: #fff;
        max-width: 350px;
        border: 1px solid #ddd;
        padding-left: 0px;
        cursor: pointer;
    }

    ul#ui-id-1 li,
    ul#ui-id-2 li {
        list-style: none;
        border: 0.5px solid #ddd;
        padding-left: 0px;
        padding: 5px;
    }
    </style>
</head>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper">
        <?php include "header.php"; ?>
        <?php include "sidebar.php"; ?>
        <div class="page-wrapper">

            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Eco Products</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Products</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                if(isset($_GET['id'])) {
                    $prodId = $_GET['id'];
                    $caseQuery = "SELECT sub_product.p_id, products.prod_name, products.prod_description, sub_product.m_id, sub_product.p_id, sub_product.datasheets, sub_product.drawings, sub_product.other_docs, materials.mat_name, producttype.prd_type_name
                    FROM sub_product
                    RIGHT JOIN products ON sub_product.p_id=products.prod_id 
                    RIGHT JOIN materials ON sub_product.m_id=materials.mat_id
                    RIGHT JOIN producttype ON sub_product.t_id=producttype.prdtype_id
                    where sub_product.p_id='$prodId'";
                    $count = 0;
                    $query = $connection->query($caseQuery);
                    if($query->num_rows > 0){
                        while($row = $query->fetch_assoc()){
                    ?>

            <div class="container-fluid">
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <?php 
                                if($count == 0){?>
                                <h3 class="card-title"><?=$row['prod_name']?></h3>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <h4 class="box-title m-t-40">Product description</h4>
                                        <p><?=$row['description']?></p>
                                    </div>
                                <?php
                                $count++;
                                }
                                ?>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                        <ul class="list-unstyled">
                                            <li><i class="fa fa-check text-success">Material: </i>
                                                <?= $row['mat_name'] ?></li>
                                            <li><i class="fa fa-check text-success">product Type: </i>
                                                <?= $row['prd_type_name'] ?>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h3 class="box-title m-t-40">Drawings</h3>
                                                <ul class="list-unstyled">
                                                    <?php
                                            if ($row['drawings'] != '') {
                                                $imageNames = explode(":", $row['drawings']);
                                                for($i = 0; $i< count($imageNames); $i++){
                                                    echo "<li><i class='fa fa-check text-success'></i><a href='download.php?file=" . urlencode($imageNames[$i]) . "'>Download $imageNames[$i]</a></li>";
                                                }
                                                // print_r($imageNames);
                                                // $abc = $imageNames[0];
                                            }
                                            else{
                                                echo "No drowings added.";
                                            }
                                            ?>
                                                </ul>
                                            </div>
                                            <div class="col-md-4">
                                                <h3 class="box-title m-t-40">Datasheets</h3>
                                                <ul class="list-unstyled">
                                                    <?php
                                            if ($row['drawings'] != '') {
                                                $imageNames = explode(":", $row['datasheets']);
                                                for($i = 0; $i< count($imageNames); $i++){
                                                    echo "<li><i class='fa fa-check text-success'></i><a href='download.php?file=" . urlencode($imageNames[$i]) . "'>Download $imageNames[$i]</a></li>";
                                                }
                                                // print_r($imageNames);
                                                // $abc = $imageNames[0];
                                            }
                                            else{
                                                echo "No datasheet added.";
                                            }
                                            ?>
                                                </ul>
                                            </div>
                                            <div class="col-md-4">
                                                <h3 class="box-title m-t-40">Other Documents</h3>
                                                <ul class="list-unstyled">
                                                    <?php
                                            if ($row['drawings'] != '') {
                                                $imageNames = explode(":", $row['other_docs']);
                                                for($i = 0; $i< count($imageNames); $i++){
                                                    echo "<li><i class='fa fa-check text-success'></i><a href='download.php?file=" . urlencode($imageNames[$i]) . "'>Download $imageNames[$i]</a></li>";
                                                }
                                                // print_r($imageNames);
                                                // $abc = $imageNames[0];
                                            }
                                            else{
                                                echo "No document added.";
                                            }
                                            ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
            <?php
                    // print_r($row);
                }
            }
                else if(!isset($_GET['id'])) {
                    header("Location:products.php");
                }
            }
        
            ?>
            <div class="container-fluid">
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="api/web/save-product.php" method="POST" enctype="multipart/form-data"
                                    class="form-horizontal mt-4" id="prduct_form">
                                    <input type="hidden" id="prodId" name="prodId" value="<?= $prodId ?>">
                                    <div class="form-group">
                                        <label>Material Type</label>
                                        <input required id="matType" name="matType" required class="form-control" />
                                        <input type="hidden" id="matTypeId" name="matTypeId" required
                                            class="form-control" />
                                    </div>

                                    <div class="form-group">
                                        <label>Product Type</label>
                                        <input required id="prdType" name="prdType" required class="form-control" />
                                        <input type="hidden" id="prdTypeId" name="prdTypeId" required
                                            class="form-control" />
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Images</label>
                                        <div class="dropzone" id="myDropzone" name="file1"></div>
                                    </div>

                                    <div class="form-group">
                                        <label>Datasheets</label>
                                        <div class="dropzone" id="myDropzone2" name="file2"></div>
                                    </div>

                                    <div class="form-group">
                                        <label>Documents</label>
                                        <div class="dropzone" id="myDropzone3" name="file3"></div>
                                    </div>

                                    <button type="submit" id="submit_btn" class="btn btn-success">
                                        Submit
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <footer class="footer text-center">
            All Rights Reserved by Xtreme admin. Designed and Developed by <a
                href="https://wrappixel.com/">WrapPixel</a>.
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- customizer Panel -->
    <!-- ============================================================== -->
    <aside class="customizer">

        <div class="customizer-body">
            <ul class="nav customizer-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
                        aria-controls="pills-home" aria-selected="true"><i class="mdi mdi-wrench font-20"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#chat" role="tab"
                        aria-controls="chat" aria-selected="false"><i class="mdi mdi-message-reply font-20"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab"
                        aria-controls="pills-contact" aria-selected="false"><i
                            class="mdi mdi-star-circle font-20"></i></a>
                </li>
            </ul>

        </div>
    </aside>
    <div class="chat-windows"></div>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script src="assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="dist/js/app.min.js"></script>
    <script src="dist/js/app.init.light-sidebar.js"></script>
    <script src="dist/js/app-style-switcher.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <script src="assets/libs/dropzone/dist/min/dropzone.min.js"></script>

    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <script src="sweetalert/sweetalert.min.js"></script>


    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js">
    </script>

    <script>
    $(document).ready(function() {
        $(function() {
            $("#matType").autocomplete({
                source: "api/web/filter-mat.php",
                select: function(event, ui) {
                    event.preventDefault();
                    $("#matType").val(ui.item.value);
                    $("#matTypeId").val(ui.item.id);
                }
            });
        });

        $(function() {
            $("#prdType").autocomplete({
                source: "api/web/filter-prd.php",
                select: function(event, ui) {
                    event.preventDefault();
                    $("#prdType").val(ui.item.value);
                    $("#prdTypeId").val(ui.item.id);
                }
            });
        });
    })

    Dropzone.autoDiscover = false;
    $(document).ready(() => {
        const dropzones = [];
        $('.dropzone').each(function(i, el) {
            const name = $(el).attr('name');
            var myDropzone = new Dropzone(el, {
                url: 'api/web/save-product.php',
                autoProcessQueue: false,
                autoDiscover: false,
                uploadMultiple: false,
                parallelUploads: 15,
                maxFiles: 5,
                paramName: name,
                addRemoveLinks: true,
                chunking: true,
                retryChunks: true,
                parallelChunkUploads: true,
            });
            dropzones.push(myDropzone)
        });


        document.querySelector("button[type=submit]").addEventListener("click", function(e) {
            matType = $('#matTypeId').val();
            matTypeName = $('#matType').val();
            if (matType == '') {
                $.ajax({
                    type: "POST",
                    async: false,
                    url: 'api/web/get-matid.php',
                    data: {
                        mat_name: matTypeName
                    },
                    success: function(data) {
                        console.log(data);
                        $('#matTypeId').val(data);
                    }
                });
            }
            prdType = $('#prdType').val();
            prdTypeId = $('#prdTypeId').val();
            if (prdTypeId == '') {
                console.log("here");
                $.ajax({
                    type: "POST",
                    async: false,
                    url: 'api/web/get-prdtypeid.php',
                    data: {
                        prd_name: prdType
                    },
                    success: function(data) {
                        console.log(data);
                        $('#prdTypeId').val(data);
                    }
                });
            }
            var prodId = $('#prodId').val()
            console.log(prodId);
            if (prodId == '') {
                e.preventDefault();
                swal({
                    title: "Error",
                    text: "Failed",
                    icon: "warning"
                });
            } else {
                console.log('main')
                e.preventDefault();
                e.stopPropagation();
                let form = new FormData($('form')[0]);

                dropzones.forEach(dropzone => {
                    let {
                        paramName
                    } = dropzone.options;
                    // console.log(paramName);
                    dropzone.files.forEach((file, i) => {
                        form.append(paramName + '[' + i + ']', file)
                    })
                    //             dzClosure.processQueue();
                });
                $.ajax({
                    method: 'POST',
                    url: 'api/web/save-product.php',
                    data: form,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if (jQuery.trim(data) === "SQLSuccess") {
                            console.log(data);
                            swal({
                                title: "Success",
                                text: "Data Added.",
                                icon: "success"
                            });
                            dropzones.forEach(dropzone => {

                                dropzone.files.forEach(function(file) {
                                    file.previewElement.remove();
                                });

                                $('.dropzone').removeClass('dz-started');
                            })
                            document.getElementById("prduct_form").reset();
                        }
                    },
                    error: function(error) {
                        swal({
                            title: "Error",
                            text: "Error AJAX not working: " + error,
                            icon: "error"
                        });
                    }
                });
            }
        });
    })
    </script>

</body>

</html>
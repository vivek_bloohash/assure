<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                <li>
                    <!-- User Profile-->
                    <div class="user-profile d-flex no-block dropdown m-t-20">
                        <div class="user-logo">
                            <img src="assets/images/users/1.jpg" alt="users" class="rounded-circle" width="40" /> </div>
                        <div class="user-content hide-menu m-l-10">
                            <a href="javascript:void(0)" class="" id="Userdd" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <h5 class="m-b-0 user-name font-medium">Steave Jobs <i class="fa fa-angle-down"></i></h5>
                                <span class="op-5 user-email">varun@gmail.com</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Userdd">
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-user m-r-5 m-l-5"></i> My Profile</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-wallet m-r-5 m-l-5"></i> My Balance</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-email m-r-5 m-l-5"></i> Inbox</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-settings m-r-5 m-l-5"></i> Account Setting</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                            </div>
                        </div>
                    </div>
                    <!-- End User Profile-->
                </li>

                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="add-customer.php" aria-expanded="false"><i style="font-size:16px" class="fa fa-user-plus"></i><span class="hide-menu">Add Customer</span></a></li>




                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="customer-enquiry.php" aria-expanded="false"><i style="font-size:16px" class="fas fa-edit"></i><span class="hide-menu">Add Customer Enquiry</span></a></li>

                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="add-new-product.php" aria-expanded="false"><i style="font-size:16px" class="fas fa-user-plus"></i><span class="hide-menu">Add New Product</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="products.php" aria-expanded="false"><i style="font-size:16px" class="fas fa-user-plus"></i><span class="hide-menu">All Products</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="add-new-material-type.php" aria-expanded="false"><i style="font-size:16px" class="fas fa-user-plus"></i><span class="hide-menu">Add New Material</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="add-new-product-type.php" aria-expanded="false"><i style="font-size:16px" class="fas fa-user-plus"></i><span class="hide-menu">Add New Product Type</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="import-vendors.php" aria-expanded="false"><i style="font-size:16px" class="fas fa-calendar-plus"></i><span class="hide-menu">Import Vendors</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="vendors.php" aria-expanded="false"><i style="font-size:16px" class="fas fa-hospital-alt"></i><span class="hide-menu">All vendors</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="display_enquiries.php" aria-expanded="false"><i style="font-size:16px" class="fas fa-folder"></i><span class="hide-menu">Display Enquiries</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="display_customer.php" aria-expanded="false"><i style="font-size:16px" class="fa fa-users"></i><span class="hide-menu">Display Customers</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="add-new-project.php" aria-expanded="false"><i style="font-size:16px" class="fa fa-users"></i><span class="hide-menu">Add New Project</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="projects.php" aria-expanded="false"><i style="font-size:16px" class="fa fa-users"></i><span class="hide-menu">All Projects</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="add-new-employee.php" aria-expanded="false"><i style="font-size:16px" class="fas fa-calendar-plus"></i><span class="hide-menu">Add New Employee</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="all-employees.php" aria-expanded="false"><i style="font-size:16px" class="fas fa-calendar-plus"></i><span class="hide-menu">All Employees</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="add-customer.php" aria-expanded="false"><i style="font-size:16px" class="fa fa-power-off"></i><span class="hide-menu">Logout</span></a></li>



            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<?php
   session_start();
   if(!isset($_SESSION['logged_in'])){
       header("Location:index.php");
   }
   
   include 'api/web/dbconfig.php';
   ?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Customer Enquiry</title>
    <link rel="canonical" href="https://www.wrappixel.com/templates/xtremeadmin/" />
    <!-- Custom CSS -->
    <link href="dist/css/style.min.css" rel="stylesheet">
    <link href="sweetalert/sweetalert.css" rel="stylesheet">
    <link href="assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="assets/libs/dropzone/dist/min/dropzone.min.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css"
        href="assets/extra-libs/datatables.net-bs4/css/responsive.dataTables.min.css">
    <!-- <link href="sweetalert/sweetalert.css" rel="stylesheet"> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <style>
    .submit_pg {
        margin: 0 auto;
        display: block;
        margin-bottom: 40px;
    }

    .hidden {
        display: none;
    }
    </style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php include "header.php"; ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php include "sidebar.php"; ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <!-- <h4 class="page-title">Customer Enquiry</h4> -->
                        <!-- <div class="d-flex align-items-center">
                        <nav aria-label="breadcrumb">
                        	<ol class="breadcrumb">
                        		<li class="breadcrumb-item"><a href="#">Home</a></li>
                        		<li class="breadcrumb-item active" aria-current="page">Customer Enquiry</li>
                        	</ol>
                        </nav>
                        </div> -->
                    </div>
                    <div class="col-7 align-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <div class="m-r-10">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-3"><a href="display_customer.php" class="btn btn-sm btn-link m-b-5"><i
                                class="fa fa-arrow-left m-r-5"></i> Back to enquiry</a></div>
                </div>
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <!--   <div class="card-header bg-info">
                     <h4 class="m-b-0 text-white">Customer Details</h4>
                     </div> -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header bg-info">
                                <h4 class="m-b-0 text-white">Customer Details</h4>
                            </div>
                            <form class="form-horizontal">
                                <div class="form-body">
                                    <div class="card-body">
                                        <h4 class="card-title"></h4>
                                    </div>
                                    <!-- <hr class="m-t-0 m-b-40"> -->
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label
                                                        class="control-label text-right text-dark col-md-3 m-t-5">Customer:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c1"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label
                                                        class="control-label text-right text-dark col-md-3 m-t-5">Division:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c2"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label
                                                        class="control-label text-right text-dark col-md-3 m-t-5">Location:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c3"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label
                                                        class="control-label text-right text-dark col-md-3 m-t-5">Work:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c4"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="control-label text-right text-dark col-md-3 m-t-5">
                                                        Pin:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c5"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label
                                                        class="control-label text-right text-dark col-md-3 m-t-5">Office:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c6"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label
                                                        class="control-label text-right text-dark col-md-3 m-t-5">Office
                                                        Pin:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c7"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label
                                                        class="control-label text-right text-dark col-md-3 m-t-5">Contact:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c8"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label
                                                        class="control-label text-right text-dark col-md-3 m-t-5">Department:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c9"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label
                                                        class="control-label text-right text-dark col-md-3 m-t-5">Email:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c10"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label
                                                        class="control-label text-right text-dark col-md-3 m-t-5">Mobile1:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c11"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label
                                                        class="control-label text-right text-dark col-md-3 m-t-5">Mobile2:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c12"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label
                                                        class="control-label text-right text-dark col-md-3 m-t-5">Whatsapp:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c13"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label
                                                        class="control-label text-right text-dark col-md-3 m-t-5">Remark:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c14"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label
                                                        class="control-label text-right text-dark col-md-3 m-t-5">Enquiry
                                                        No:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark" id="c15"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label
                                                        class="control-label text-right text-dark col-md-3 m-t-5">Product:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static text-dark" id="c16"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row">
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label
                                                        class="control-label text-right text-dark col-md-3 m-t-5">Added
                                                        At:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control text-dark " id="c17"> </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label
                                                        class="control-label text-right text-dark col-md-3 m-t-5">Upload
                                                        Documents:</label>
                                                    <div class="dropzone" id="customer-upload">
                                                        <!--  <form action="api/web/upload.php" class="dropzone" id="customer-upload"></form> -->
                                                    </div>
                                                    <!-- <div class="col-md-9">
                                             <p class="form-control text-dark " id="c18">  </p>
                                             </div> -->
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <!--  <h4 class="card-title">Address</h4> -->
                                    </div>
                                    <!--  <hr class="m-t-0 m-b-40"> -->
                                    <!--  <hr> -->
                                    <div class="form-actions">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <!-- <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn btn-danger"> <i class="fa fa-pencil"></i> Edit</button>
                                                <button type="button" class="btn btn-dark">Cancel</button>
                                                </div> -->
                                                    </div>
                                                </div>
                                                <div class="col-md-6"> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-md-2">
                                    <form method="post" action="select-vendors.php">
                                        <input type="hidden" name="enqueiry_id" value="<?=$_GET['id']?>">
                                        <input type="submit" value="Select Vendors" class="btn btn-success submit_pg">
                                    </form>
                                </div>
                                <div class="col-md-2">
                                    <form method="post" action="enter-prices.php" class="v_list">
                                        <input type="hidden" name="enqueiry_id" value="<?=$_GET['id']?>">
                                        <input type="submit" value="Enter Prices" class="btn btn-success submit_pg">
                                    </form>
                                </div>
                                <div class="col-md-2">
                                    <form method="post" action="show-price-chart.php" class="p_list">
                                        <input type="hidden" name="enqueiry_id" value="<?=$_GET['id']?>">
                                        <input type="submit" value="View Chart" class="btn btn-success submit_pg">
                                    </form>
                                </div>
                                <div class="col-md-6">
                                    <button type="button" id="formButton" class="btn btn-success text-center"
                                        style="display: block; margin: 0 auto;">Add remark</button>
                                    <form id="add-review-form" url="api/web/save-task-review.php"
                                        style="margin-top: 30px">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="control-label text-right text-dark col-md-3 m-t-5">Add
                                                    remark:</label>
                                                    <div class="col-md-9">
                                                    <textarea rows="4" cols="50" class="form-control text-dark col-md-9 m-t-5"
                                                            name="review" id="review"></textarea>
                                                        <input class="form-control text-dark col-md-9 m-t-5" type="hidden"
                                                            name="username" id="username" value="<?= $_SESSION['username']?>"> 
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <div class="col-md-9">
                                                        <input class="form-control text-dark col-md-9 m-t-5"
                                                            value="<?=$_GET['id']?>" type="hidden" name="enq_id"
                                                            id="enq_id">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <input type="submit" value="Save" class="btn btn-success submit_pg">
                                    </form>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4>Remarks</h4>
                                            <ul class="timeline timeline-left postList">
                                                <?php
                                                    $e_id = $_GET['id'];
                                                    $query = $connection->query("SELECT * FROM `enquiry_review` INNER JOIN users ON enquiry_review.username = users.email where enquiry_review.enq_id = '$e_id' ORDER BY enquiry_review.id DESC LIMIT 2");
                                                if($query->num_rows > 0){
                                                while($row = $query->fetch_assoc()){
                                                $postID = $row['id'];
                                                ?>
                                                <li class="timeline-inverted timeline-item">
                                                    <div class="timeline-badge success"><img
                                                            src="assets/images/users/1.jpg" alt="img" class="img-fluid">
                                                    </div>
                                                    <div class="timeline-panel">
                                                        <div class="timeline-heading">
                                                            <h4 class="timeline-title"><?= $row['name'] ?> </h4>
                                                            <p><small class="text-muted"><i class="fa fa-clock-o"></i>
                                                            
                                                            <?php echo date('F j, Y, g:i a', strtotime($row['added_at']));?>
                                                            </small> </p>
                                                        </div>
                                                        <div class="timeline-body">
                                                            <p><?php echo $row['review']; ?></p>
                                                        </div>
                                                    </div>
                                                </li>
                                                <?php } ?>
                                                <div class="show_more_main" id="show_more_main<?php echo $postID; ?>">
                                                    <span id="<?php echo $postID; ?>" class="show_more btn btn-default"
                                                        title="Load more posts">Show more</span>
                                                    <span class="loding" style="display: none;"><span
                                                            class="loding_txt">Loading...</span></span>
                                                </div>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Right sidebar -->
            <!-- ============================================================== -->
            <!-- .right-sidebar -->
            <!-- ============================================================== -->
            <!-- End Right sidebar -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <?php include "footer.php"; ?>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- customizer Panel -->
    <!-- ============================================================== -->
    <aside class="customizer">
        <a href="javascript:void(0)" class="service-panel-toggle"><i class="fa fa-spin fa-cog"></i></a>
        <div class="customizer-body">
            <ul class="nav customizer-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
                        aria-controls="pills-home" aria-selected="true"><i class="mdi mdi-wrench font-20"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#chat" role="tab"
                        aria-controls="chat" aria-selected="false"><i class="mdi mdi-message-reply font-20"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab"
                        aria-controls="pills-contact" aria-selected="false"><i
                            class="mdi mdi-star-circle font-20"></i></a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <!-- Tab 1 -->
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="p-15 border-bottom">
                        <!-- Sidebar -->
                        <h5 class="font-medium m-b-10 m-t-10">Layout Settings</h5>
                        <div class="custom-control custom-checkbox m-t-10">
                            <input type="checkbox" class="custom-control-input" name="theme-view" id="theme-view">
                            <label class="custom-control-label" for="theme-view">Dark Theme</label>
                        </div>
                        <div class="custom-control custom-checkbox m-t-10">
                            <input type="checkbox" class="custom-control-input sidebartoggler" name="collapssidebar"
                                id="collapssidebar">
                            <label class="custom-control-label" for="collapssidebar">Collapse Sidebar</label>
                        </div>
                        <div class="custom-control custom-checkbox m-t-10">
                            <input type="checkbox" class="custom-control-input" name="sidebar-position"
                                id="sidebar-position">
                            <label class="custom-control-label" for="sidebar-position">Fixed Sidebar</label>
                        </div>
                        <div class="custom-control custom-checkbox m-t-10">
                            <input type="checkbox" class="custom-control-input" name="header-position"
                                id="header-position">
                            <label class="custom-control-label" for="header-position">Fixed Header</label>
                        </div>
                        <div class="custom-control custom-checkbox m-t-10">
                            <input type="checkbox" class="custom-control-input" name="boxed-layout" id="boxed-layout">
                            <label class="custom-control-label" for="boxed-layout">Boxed Layout</label>
                        </div>
                    </div>
                    <div class="p-15 border-bottom">
                        <!-- Logo BG -->
                        <h5 class="font-medium m-b-10 m-t-10">Logo Backgrounds</h5>
                        <ul class="theme-color">
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-logobg="skin1"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-logobg="skin2"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-logobg="skin3"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-logobg="skin4"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-logobg="skin5"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-logobg="skin6"></a></li>
                        </ul>
                        <!-- Logo BG -->
                    </div>
                    <div class="p-15 border-bottom">
                        <!-- Navbar BG -->
                        <h5 class="font-medium m-b-10 m-t-10">Navbar Backgrounds</h5>
                        <ul class="theme-color">
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-navbarbg="skin1"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-navbarbg="skin2"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-navbarbg="skin3"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-navbarbg="skin4"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-navbarbg="skin5"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-navbarbg="skin6"></a></li>
                        </ul>
                        <!-- Navbar BG -->
                    </div>
                    <div class="p-15 border-bottom">
                        <!-- Logo BG -->
                        <h5 class="font-medium m-b-10 m-t-10">Sidebar Backgrounds</h5>
                        <ul class="theme-color">
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-sidebarbg="skin1"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-sidebarbg="skin2"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-sidebarbg="skin3"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-sidebarbg="skin4"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-sidebarbg="skin5"></a></li>
                            <li class="theme-item"><a href="javascript:void(0)" class="theme-link"
                                    data-sidebarbg="skin6"></a></li>
                        </ul>
                        <!-- Logo BG -->
                    </div>
                </div>
                <!-- End Tab 1 -->
                <!-- Tab 2 -->
                <div class="tab-pane fade" id="chat" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <ul class="mailbox list-style-none m-t-20">
                        <li>
                            <div class="message-center chat-scroll">
                                <a href="javascript:void(0)" class="message-item" id='chat_user_1' data-user-id='1'>
                                    <span class="user-img"> <img src="assets/images/users/1.jpg" alt="user"
                                            class="rounded-circle"> <span
                                            class="profile-status online pull-right"></span> </span>
                                    <span class="mail-contnet">
                                        <h5 class="message-title">Pavan kumar</h5>
                                        <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30
                                            AM</span>
                                    </span>
                                </a>
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_2' data-user-id='2'>
                                    <span class="user-img"> <img src="assets/images/users/2.jpg" alt="user"
                                            class="rounded-circle"> <span class="profile-status busy pull-right"></span>
                                    </span>
                                    <span class="mail-contnet">
                                        <h5 class="message-title">Sonu Nigam</h5>
                                        <span class="mail-desc">I've sung a song! See you at</span> <span
                                            class="time">9:10 AM</span>
                                    </span>
                                </a>
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_3' data-user-id='3'>
                                    <span class="user-img"> <img src="assets/images/users/3.jpg" alt="user"
                                            class="rounded-circle"> <span class="profile-status away pull-right"></span>
                                    </span>
                                    <span class="mail-contnet">
                                        <h5 class="message-title">Arijit Sinh</h5>
                                        <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span>
                                    </span>
                                </a>
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_4' data-user-id='4'>
                                    <span class="user-img"> <img src="assets/images/users/4.jpg" alt="user"
                                            class="rounded-circle"> <span
                                            class="profile-status offline pull-right"></span> </span>
                                    <span class="mail-contnet">
                                        <h5 class="message-title">Nirav Joshi</h5>
                                        <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02
                                            AM</span>
                                    </span>
                                </a>
                                <!-- Message -->
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_5' data-user-id='5'>
                                    <span class="user-img"> <img src="assets/images/users/5.jpg" alt="user"
                                            class="rounded-circle"> <span
                                            class="profile-status offline pull-right"></span> </span>
                                    <span class="mail-contnet">
                                        <h5 class="message-title">Sunil Joshi</h5>
                                        <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02
                                            AM</span>
                                    </span>
                                </a>
                                <!-- Message -->
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_6' data-user-id='6'>
                                    <span class="user-img"> <img src="assets/images/users/6.jpg" alt="user"
                                            class="rounded-circle"> <span
                                            class="profile-status offline pull-right"></span> </span>
                                    <span class="mail-contnet">
                                        <h5 class="message-title">Akshay Kumar</h5>
                                        <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02
                                            AM</span>
                                    </span>
                                </a>
                                <!-- Message -->
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_7' data-user-id='7'>
                                    <span class="user-img"> <img src="assets/images/users/7.jpg" alt="user"
                                            class="rounded-circle"> <span
                                            class="profile-status offline pull-right"></span> </span>
                                    <span class="mail-contnet">
                                        <h5 class="message-title">Pavan kumar</h5>
                                        <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02
                                            AM</span>
                                    </span>
                                </a>
                                <!-- Message -->
                                <!-- Message -->
                                <a href="javascript:void(0)" class="message-item" id='chat_user_8' data-user-id='8'>
                                    <span class="user-img"> <img src="assets/images/users/8.jpg" alt="user"
                                            class="rounded-circle"> <span
                                            class="profile-status offline pull-right"></span> </span>
                                    <span class="mail-contnet">
                                        <h5 class="message-title">Varun Dhavan</h5>
                                        <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02
                                            AM</span>
                                    </span>
                                </a>
                                <!-- Message -->
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- End Tab 2 -->
                <!-- Tab 3 -->
                <div class="tab-pane fade p-15" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h6 class="m-t-20 m-b-20">Activity Timeline</h6>
                    <div class="steamline">
                        <div class="sl-item">
                            <div class="sl-left bg-success"> <i class="ti-user"></i></div>
                            <div class="sl-right">
                                <div class="font-medium">Meeting today <span class="sl-date"> 5pm</span></div>
                                <div class="desc">you can write anything </div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left bg-info"><i class="fas fa-image"></i></div>
                            <div class="sl-right">
                                <div class="font-medium">Send documents to Clark</div>
                                <div class="desc">Lorem Ipsum is simply </div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left"> <img class="rounded-circle" alt="user"
                                    src="assets/images/users/2.jpg"> </div>
                            <div class="sl-right">
                                <div class="font-medium">Go to the Doctor <span class="sl-date">5 minutes ago</span>
                                </div>
                                <div class="desc">Contrary to popular belief</div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left"> <img class="rounded-circle" alt="user"
                                    src="assets/images/users/1.jpg"> </div>
                            <div class="sl-right">
                                <div><a href="javascript:void(0)">Stephen</a> <span class="sl-date">5 minutes ago</span>
                                </div>
                                <div class="desc">Approve meeting with tiger</div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left bg-primary"> <i class="ti-user"></i></div>
                            <div class="sl-right">
                                <div class="font-medium">Meeting today <span class="sl-date"> 5pm</span></div>
                                <div class="desc">you can write anything </div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left bg-info"><i class="fas fa-image"></i></div>
                            <div class="sl-right">
                                <div class="font-medium">Send documents to Clark</div>
                                <div class="desc">Lorem Ipsum is simply </div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left"> <img class="rounded-circle" alt="user"
                                    src="assets/images/users/4.jpg"> </div>
                            <div class="sl-right">
                                <div class="font-medium">Go to the Doctor <span class="sl-date">5 minutes ago</span>
                                </div>
                                <div class="desc">Contrary to popular belief</div>
                            </div>
                        </div>
                        <div class="sl-item">
                            <div class="sl-left"> <img class="rounded-circle" alt="user"
                                    src="assets/images/users/6.jpg"> </div>
                            <div class="sl-right">
                                <div><a href="javascript:void(0)">Stephen</a> <span class="sl-date">5 minutes ago</span>
                                </div>
                                <div class="desc">Approve meeting with tiger</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Tab 3 -->
            </div>
        </div>
    </aside>
    <div class="chat-windows"></div>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="dist/js/app.min.js"></script>
    <script src="dist/js/app.init.light-sidebar.js"></script>
    <script src="dist/js/app-style-switcher.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <script src="sweetalert/sweetalert.min.js"></script>
    <script src="assets/extra-libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="assets/extra-libs/datatables.net-bs4/js/dataTables.responsive.min.js"></script>
    <script src="assets/libs/dropzone/dist/min/dropzone.min.js"></script>
    <style>
    #add-review-form {
        display: none;
    }
    div.show_more_main {
    text-align: center;
}
    </style>
    <script>
    var dropzone = new Dropzone('#customer-upload', {
        // previewTemplate: document.querySelector('#preview-template').innerHTML,
        parallelUploads: 4,
        url: "api/web/upload.php?id=<?=$_GET['id']?>",
        thumbnailHeight: 120,
        thumbnailWidth: 120,
        maxFilesize: 3,
        filesizeBase: 1000,
        thumbnail: function(file, dataUrl) {
            if (file.previewElement) {
                file.previewElement.classList.remove("dz-file-preview");
                var images = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
                for (var i = 0; i < images.length; i++) {
                    var thumbnailElement = images[i];
                    thumbnailElement.alt = file.name;
                    thumbnailElement.src = dataUrl;
                }
                setTimeout(function() {
                    file.previewElement.classList.add("dz-image-preview");
                }, 1);
            }
        }
    });
    </script>
    <script>
    $.ajax({
        type: "GET",
        url: "api/web/view_enquiry.php",
        data: {
            id: "<?=$_GET['id']?>"
        }, // serializes the form's elements.
        success: function(data) {
            document.getElementById("c1").innerHTML = data[0]["cust_name"];
            document.getElementById("c2").innerHTML = data[0]["division"];
            document.getElementById("c3").innerHTML = data[0]["location"];
            document.getElementById("c4").innerHTML = data[0]["work_address"];
            document.getElementById("c5").innerHTML = data[0]["work_pin"];
            document.getElementById("c6").innerHTML = data[0]["office_address"];
            document.getElementById("c7").innerHTML = data[0]["office_pin"];
            document.getElementById("c8").innerHTML = data[0]["contact_person"];
            document.getElementById("c9").innerHTML = data[0]["department"];
            document.getElementById("c10").innerHTML = data[0]["email"];
            document.getElementById("c11").innerHTML = data[0]["mobile1"];
            document.getElementById("c12").innerHTML = data[0]["mobile2"];
            document.getElementById("c13").innerHTML = data[0]["whatsapp"];
            document.getElementById("c14").innerHTML = data[0]["remark"];
            document.getElementById("c15").innerHTML = data[0]["enquiry_no"];
            document.getElementById("c16").innerHTML = data[0]["product_details"];
            document.getElementById("c17").innerHTML = data[0]["added_at"];
            var vendor_list = data[0]["vendors"];
            var price_list = data[0]["prices"];
            console.log(vendor_list, price_list);
            if (vendor_list != null) {
                $(".v_list").show();
            }

            if (price_list != null) {
                $(".p_list").show();
            }
        }
    });


    $.ajax({
        type: "GET",
        url: "api/web/view_enquiry.php",
        data: {
            id: "<?=$_GET['id']?>"
        }, // serializes the form's elements.
        success: function(data) {
            document.getElementById("c1").innerHTML = data[0]["cust_name"];
            document.getElementById("c2").innerHTML = data[0]["division"];
            document.getElementById("c3").innerHTML = data[0]["location"];
            document.getElementById("c4").innerHTML = data[0]["work_address"];
            document.getElementById("c5").innerHTML = data[0]["work_pin"];
            document.getElementById("c6").innerHTML = data[0]["office_address"];
            document.getElementById("c7").innerHTML = data[0]["office_pin"];
            document.getElementById("c8").innerHTML = data[0]["contact_person"];
            document.getElementById("c9").innerHTML = data[0]["department"];
            document.getElementById("c10").innerHTML = data[0]["email"];
            document.getElementById("c11").innerHTML = data[0]["mobile1"];
            document.getElementById("c12").innerHTML = data[0]["mobile2"];
            document.getElementById("c13").innerHTML = data[0]["whatsapp"];
            document.getElementById("c14").innerHTML = data[0]["remark"];
            document.getElementById("c15").innerHTML = data[0]["enquiry_no"];
            document.getElementById("c16").innerHTML = data[0]["product_details"];
            document.getElementById("c17").innerHTML = data[0]["added_at"];
            var vendor_list = data[0]["vendors"];
            var price_list = data[0]["prices"];


        }
    });



    $(document).ready(function() {
        $("#formButton").click(function() {
            $("#add-review-form").toggle();
        });

        $(document).on('click', '.show_more', function() {
            var ID = $(this).attr('id');
            $('.show_more').hide();
            $('.loding').show();
            $.ajax({
                type: 'POST',
                url: 'api/web/load-review.php',
                data: {
                    id: ID,
                    enq_id: "<?=$_GET['id']?>"
                },
                success: function(html) {
                    $('#show_more_main' + ID).remove();
                    $('.postList').append(html);
                }
            });
        });
    });

    $("#add-review-form").submit(function(e) {
        var form = $(this);
        var url = form.attr('action');
        var formCopy = document.getElementById('add-review-form');
        var formData = new FormData(formCopy);
        //var formData = 
        $.ajax({
            contentType: false,
            processData: false,
            type: "POST",
            url: "api/web/save-enq-review.php",
            data: formData, // serializes the form's elements.

            success: function(data) {
                //alert(data);
                //document.getElementById("test-update-spinner").style.display = "none";
                if (data == 'Success') {
                    swal({
                        title: "Done!",
                        text: "Remark was inserted successfully",
                        type: "success"
                    }, function() {
                        window.location = window.location;
                    });
                } else {
                    swal("Failed!", data, "error");
                }
                /*document.getElementById('info_message').innerHTML = data;*/ // show response from the php script.
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
    </script>
    <style>
    /*#customer-upload{
         width: 100%;
         }*/
    </style>
</body>

</html>